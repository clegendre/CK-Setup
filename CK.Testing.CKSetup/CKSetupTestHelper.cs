using CK.Core;
using CK.Testing.CKSetup;
using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing
{
    /// <summary>
    /// Exposes the <see cref="ICKSetupTestHelper"/> mixin test helper.
    /// </summary>
    public class CKSetupTestHelper : CKSetup.ICKSetupTestHelperCore
    {
        readonly ICKSetupDriver _driver;

        class Driver : ICKSetupDriver
        {
            readonly IMonitorTestHelper _monitor;
            readonly NormalizedPath _localStoresFolder;
            readonly NormalizedPath _defaultStorePath;
            readonly Uri _defaultStoreUrl;
            readonly bool _generateComponentsMode;
            bool _launchDebug;
            bool _forceSetup;
            // This CK.Testing.CKSetup targets net461;netcoreapp2.1.
            // We use it to configure the PreferredTargetRuntimes when none is specified.
            TargetFramework _thisTargetFramework;

            public Driver( ITestHelperConfiguration config, IMonitorTestHelper monitor )
            {
                // Sollicitates HttpClient to force assembly load here instead of inside Run:
                // when TestHelpers are instanciated we are in CK.WeakAssemblyNameResolver.
                new System.Net.Http.HttpRequestMessage();

                _monitor = monitor;
                _localStoresFolder = monitor.TestProjectFolder.AppendPart( "LocalStores" );

                AutoRegisterLocalComponentPaths = config.GetMultiPaths( "CKSetup/AutoRegisterLocalComponentPaths" ).ToList();
                if( AutoRegisterLocalComponentPaths.Count > 0 )
                {
                    _generateComponentsMode = true;
                }
                else _generateComponentsMode = config.GetBoolean( "CKSetup/GenerateComponentsMode" ) ?? false;

                var storeConf = config.GetConfigValue( "CKSetup/DefaultStorePath" );
                if( storeConf == null )
                {
                    if( _generateComponentsMode ) _defaultStorePath = _localStoresFolder.Combine( "Default" );
                    else _defaultStorePath = Facade.DefaultStorePath;
                }
                else
                {
                    if( storeConf.Value.Value.Equals( "<shared>", StringComparison.OrdinalIgnoreCase )
                        || storeConf.Value.Value.Equals( "(shared)", StringComparison.OrdinalIgnoreCase ) )
                    {
                        _defaultStorePath = Facade.DefaultStorePath;
                    }
                    else _defaultStorePath = storeConf.Value.GetValueAsPath();
                }

                WorkingDirectory = config.GetPath( "CKSetup/WorkingDirectory" ) ?? new NormalizedPath();

                DefaultBinPaths = config.GetMultiPaths( "CKSetup/DefaultBinPaths" ).ToList();


                var r = config.Get( "CKSetup/DefaultPreferredTargetRuntimes" );
                if( !String.IsNullOrWhiteSpace( r ) )
                {
                    DefaultPreferredTargetRuntimes = r.Split(';')
                                                      .Select( s => s.Trim() )
                                                      .Where( s => s.Length > 0 )
                                                      .Select( s => (TargetRuntime)Enum.Parse( typeof(TargetRuntime), s ) )
                                                      .ToArray();
                }
                else DefaultPreferredTargetRuntimes = Array.Empty<TargetRuntime>();

                var p = config.Get( "CKSetup/DefaultDependencies" );
                if( String.IsNullOrWhiteSpace( p ) ) DefaultDependencies = Array.Empty<SetupDependency>();
                else
                {
                    DefaultDependencies = p.Split( ';' )
                                            .Select( x => x.Trim() )
                                            .Where( x => x.Length > 0 )
                                            .Select( x => SetupDependency.Parse( x ) )
                                            .ToList();
                }

                var u = config.Get( "CKSetup/DefaultStoreUrl" );
                if( !String.IsNullOrWhiteSpace( u ) )
                {
                    if( !u.StartsWith( "http://" ) || !u.StartsWith( "https://" ) ) u = config.GetPath( "CKSetup/DefaultStoreUrl" );
                    _defaultStoreUrl = new Uri( u, UriKind.Absolute );
                }
                else _defaultStoreUrl = Facade.DefaultStoreUrl;

                _launchDebug = config.GetBoolean( "CKSetup/DefaultLaunchDebug" )
                                ?? config.GetBoolean( "CKSetup/LaunchDebug" )
                                ?? false;

                _forceSetup = config.GetBoolean( "CKSetup/DefaultForceSetup" )
                                ?? config.GetBoolean( "CKSetup/ForceSetup" )
                                ?? false;
            }

            public IReadOnlyList<NormalizedPath> AutoRegisterLocalComponentPaths { get; }

            public NormalizedPath LocalStoresFolder
            {
                get
                {
                    _monitor.OnlyOnce( () => _monitor.CleanupFolder( _localStoresFolder ) );
                    return _localStoresFolder;
                }
            }

            public bool GenerateComponentsMode => _generateComponentsMode;

            public NormalizedPath DefaultStorePath => _defaultStorePath;

            public NormalizedPath WorkingDirectory { get; set; }

            public IReadOnlyList<NormalizedPath> DefaultBinPaths { get; }

            public IReadOnlyList<SetupDependency> DefaultDependencies { get; }

            public bool DefaultLaunchDebug { get => _launchDebug; set => _launchDebug = value; }

            public bool DefaultForceSetup { get => _forceSetup; set => _forceSetup = value; }

            public IReadOnlyList<TargetRuntime> DefaultPreferredTargetRuntimes { get; }

            public event EventHandler<SetupRunningEventArgs> SetupRunning;

            public event EventHandler<SetupRanEventArgs> SetupRan;

            public event EventHandler<StorePathInitializationEventArgs> InitializeStorePath;

            public Uri DefaultStoreUrl => _defaultStoreUrl;

            public CKSetupRunResult Run( SetupConfiguration configuration, string storePath = null, string remoteStoreUrl = null, bool? launchDebug = false, bool? forceSetup = false )
            {
                if( configuration == null ) throw new ArgumentNullException( nameof( configuration ) );
                NormalizedPath localStore = storePath != null
                                                ? new NormalizedPath( Path.GetFullPath( storePath ) ) :
                                                _defaultStorePath;
                Uri remoteUrl = null;
                if( remoteStoreUrl != "'none'" && remoteStoreUrl != "none" )
                {
                    remoteUrl = remoteStoreUrl == null ? _defaultStoreUrl : new Uri( remoteStoreUrl, UriKind.Absolute );
                }
                if( configuration.WorkingDirectory.IsEmptyPath )
                {
                    configuration.WorkingDirectory = WorkingDirectory;
                }
                if( configuration.BinPaths.Count == 0 )
                {
                    if( DefaultBinPaths.Count > 0 )
                    {
                        _monitor.Monitor.Info( $"Using 'CKSetup/DefaultBinPaths' = {DefaultBinPaths.Select( s => s.ToString() ).Concatenate()}" );
                        configuration.BinPaths.AddRange( DefaultBinPaths );
                    }
                    else
                    {
                        _monitor.Monitor.Info( $"No 'CKSetup/DefaultBinPaths' configuration. Using current BinFolder." );
                        configuration.BinPaths.Add( _monitor.BinFolder );
                    }
                }
                if( configuration.PreferredTargetRuntimes.Count == 0 )
                {
                    if( DefaultPreferredTargetRuntimes.Count > 0 )
                    {
                        _monitor.Monitor.Info( $"Using 'CKSetup/DefaultPreferredTargetRuntimes' configuration." );
                        configuration.PreferredTargetRuntimes.AddRange( DefaultPreferredTargetRuntimes );
                    }
                    else
                    {
                        if( ThisTargetFramework != TargetFramework.None )
                        {
                            _monitor.Monitor.Info( $"No 'CKSetup/DefaultPreferredTargetRuntimes' configuration, using this CK.Testing.CKSetup assembly {ThisTargetFramework} to consider: {ThisTargetFramework.GetAllowedRuntimes().Select( r => r.ToString() ).Concatenate()}." );
                            configuration.PreferredTargetRuntimes.AddRange( ThisTargetFramework.GetAllowedRuntimes() );
                        }
                    }
                }
                // Handles {CKSetupAutoTargetProjectBinFolder} for all BinPaths.
                configuration.ProcessBinPaths( p => HandleSpecialBinPaths( p ) );

                if( configuration.Dependencies.Count == 0 && DefaultDependencies.Count > 0 )
                {
                    _monitor.Monitor.Info( $"Adding 'CKSetup/Dependencies' configuration: {DefaultDependencies.Select( s => s.ToString() ).Concatenate()}" );
                    configuration.Dependencies.AddRange( DefaultDependencies );
                }
                SetupRunningEventArgs running = new SetupRunningEventArgs( configuration, localStore, remoteUrl, launchDebug ?? _launchDebug, forceSetup ?? _forceSetup );
                SetupRunning?.Invoke( this, running );
                if( running.Cancel ) return CKSetupRunResult.None;

                // The configuration instance may have changed (pure defensive programming here: the parameter is not used below).
                configuration = running.Configuration;

                if( running.StorePath != Facade.DefaultStorePath )
                {
                    _monitor.OnlyOnce( () =>
                    {
                        using( _monitor.Monitor.OpenInfo( $"Running Setup on '{running.StorePath}' for the first time." ) )
                        {
                            if( running.StorePath == _defaultStorePath )
                            {
                                if( AutoRegisterLocalComponentPaths.Count > 0 )
                                {
                                    using( _monitor.Monitor.OpenInfo( $"Registering {AutoRegisterLocalComponentPaths.Count} components from 'CKSetup/AutoRegisterLocalComponentPaths'." ) )
                                    {
                                        RemoveComponentsFromStore( c => c.Version == CSemVer.SVersion.ZeroVersion, garbageCollectFiles: false );
                                        PublishAndAddComponentFoldersToStore( AutoRegisterLocalComponentPaths.Select( x => x.ToString() ) );
                                    }
                                }
                                else _monitor.Monitor.Info( "No 'CKSetup/AutoRegisterLocalComponentPaths' components specified." );
                            }
                            InitializeStorePath?.Invoke( this, new StorePathInitializationEventArgs( running.StorePath ) );
                        }
                    }, "Init:" + running.StorePath );
                }
                _monitor.Monitor.Info( $"Using store '{running.StorePath}'." );
                if( running.RemoteStoreUrl != null )
                {
                    _monitor.Monitor.Info( $"Using remote '{running.RemoteStoreUrl}'." );
                }
                else _monitor.Monitor.Info( $"Not using any remote." );

                using( LocalStore store = LocalStore.OpenOrCreate( _monitor.Monitor, running.StorePath, true ) )
                {
                    // If the store is not the shared one and has no PrototypeStoreUrl, we set the
                    // local shared store as the prototype.
                    if( running.StorePath != Facade.DefaultStorePath && store.PrototypeStoreUrl == null )
                    {
                        store.PrototypeStoreUrl = new Uri( Facade.DefaultStorePath, UriKind.Absolute );
                    }

                    using( IRemoteStore remote = running.RemoteStoreUrl != null && running.RemoteStoreUrl != store.PrototypeStoreUrl
                                               ? ClientRemoteStore.Create( _monitor.Monitor, running.RemoteStoreUrl, null )
                                               : null )
                    {
                        var result = CKSetupRunResult.Failed;
                        if( store != null )
                        {
                            result = Facade.DoRun( _monitor.Monitor,
                                                   store,
                                                   running.Configuration,
                                                   remote,
                                                   running.LaunchDebug,
                                                   running.ForceSetup );
                        }
                        SetupRan?.Invoke( this, new SetupRanEventArgs( running.Configuration, running.StorePath, running.RemoteStoreUrl, running.LaunchDebug, result ) );
                        return result;
                    }
                }
            }

            TargetFramework ThisTargetFramework
            {
                get
                {
                    if( _thisTargetFramework == TargetFramework.None )
                    {
                        var thisFramework = Assembly.GetExecutingAssembly().CustomAttributes
                            .Where( x => x.AttributeType.FullName == "System.Runtime.Versioning.TargetFrameworkAttribute" )
                            .Select( x => x.ConstructorArguments )
                            .Where( p => p.Count > 0 )
                            .Select( p => p[0].Value as string )
                            .FirstOrDefault();
                        TargetFramework f = TargetRuntimeOrFrameworkExtension.TryParse( thisFramework );
                        if( f == TargetFramework.None )
                        {
                            _monitor.Monitor.Warn( $"Unable to read TargetFrameworkAttribute from this CK.Testing.CKSetup assembly ({thisFramework ?? "<null>"})." );
                        }
                        _thisTargetFramework = f;
                    }
                    return _thisTargetFramework;
                }
            }

            NormalizedPath HandleSpecialBinPaths( in NormalizedPath s )
            {
                int idx = s.Parts.IndexOf( p => p == "{CKSetupAutoTargetProjectBinFolder}" );
                if( idx >= 0 )
                {
                    if( !_monitor.TestProjectName.EndsWith( ".Tests" ) )
                    {
                        _monitor.Monitor.Error( $"Unable to handle {{CKSetupAutoTargetProjectBinFolder}} placeholder: Project name {_monitor.TestProjectName} must end with '.Tests'." );
                    }
                    else
                    {
                        string[] projectNames;

                        var p1 = _monitor.TestProjectName.Substring( 0, _monitor.TestProjectName.Length - 6 );
                        if( p1.EndsWith( ".NetCore", StringComparison.OrdinalIgnoreCase ) )
                        {
                            projectNames = new[] { p1, p1.Substring( 0, p1.Length - 8 ) };
                        }
                        else projectNames = new[] { p1 };

                        var targetFolder = _monitor.TestProjectFolder.PathsToFirstPart( null, projectNames )
                                                .Where( p => Directory.Exists( p ) )
                                                .FirstOrDefault();
                        if( targetFolder.IsEmptyPath )
                        {
                            _monitor.Monitor.Error( $"Unable to resolve {{CKSetupAutoTargetProjectBinFolder}} placeholder: no '{projectNames.Concatenate( "' or '" )}' folder above {_monitor.TestProjectFolder}." );
                        }
                        else
                        {
                            targetFolder = targetFolder.AppendPart( "bin" ).AppendPart( _monitor.BuildConfiguration );
#if NET461
                            targetFolder = targetFolder.AppendPart( "net461" );
#else
                            targetFolder = targetFolder.AppendPart( "netstandard2.0" );
#endif
                            targetFolder = targetFolder.Combine( s.RemoveParts( 0, idx + 1 ) ).ResolveDots();
                            _monitor.Monitor.Info( $"BinPath '{s}' resolved to: {targetFolder}." );
                            return targetFolder;
                        }
                    }
                }
                return s;
            }

            public Facade.EnsurePublishResult EnsurePublished( string pathToFramework, bool forcePublish = false )
            {
                return Facade.EnsurePublished( _monitor.Monitor, pathToFramework, forcePublish );
            }

            public int RemoveComponentsFromStore( Func<Component, bool> exclude, bool garbageCollectFiles, string storePath = null )
            {
                using( LocalStore store = LocalStore.OpenOrCreate( _monitor.Monitor, CheckWritableStorePath( storePath ), true ) )
                {
                    int result = store.RemoveComponents( exclude );
                    if( result > 0 && garbageCollectFiles ) store.GarbageCollectFiles();
                    return result;
                }
            }

            public bool AddComponentFoldersToStore( IEnumerable<string> publishedFolders, string storePath = null, string remoteStoreUrl = null )
            {
                var monitor = _monitor.Monitor;
                var binFolders = Facade.ReadBinFolders( monitor, publishedFolders );
                if( binFolders == null ) return false;

                Uri remoteUri;
                if( remoteStoreUrl == null ) remoteUri = DefaultStoreUrl;
                else if( remoteStoreUrl == "none" ) remoteUri = null;
                else remoteUri = new Uri( remoteStoreUrl, UriKind.Absolute );

                using( LocalStore store = LocalStore.OpenOrCreate( monitor, CheckWritableStorePath( storePath ), true ) )
                {
                    // Here CheckWritableStorePath makes sure that this local store is not the shared one.
                    // If the local store has no ProtorypeUrl, we set the shared one.
                    if( store.PrototypeStoreUrl == null ) store.PrototypeStoreUrl = new Uri( Facade.DefaultStorePath, UriKind.Absolute );
                    using( IRemoteStore remote = remoteUri != null && remoteUri != store.PrototypeStoreUrl
                                               ? ClientRemoteStore.Create( _monitor.Monitor, remoteUri, null )
                                               : null )
                    {
                        return store != null && store.CreateLocalImporter( remote ).AddComponent( binFolders ).Import();
                    }
                }
            }

            public bool PublishAndAddComponentFoldersToStore( IEnumerable<string> binFrameworkPaths, bool forcePublish = false, string storePath = null, string remoteStoreUrl = null )
            {
                var safeStorePath = CheckWritableStorePath( storePath );
                var paths = new List<string>();
                foreach( var p in binFrameworkPaths )
                {
                    var r = EnsurePublished( p, forcePublish );
                    if( !r.Success ) return false;
                    paths.Add( r.FinalPath );
                }
                return AddComponentFoldersToStore( paths, safeStorePath, remoteStoreUrl );
            }

            /// <summary>
            /// Throws an exception if the storePath is eventually the shared Facade.DefaultStorePath.
            /// The shared store MUST NOT be used to register local components.
            /// </summary>
            /// <param name="storePath">The store path to challenge. When null, DefaultStorePath is used.</param>
            /// <returns>The storePath as-is or the configured DefaultStorePath if storePath is null.</returns>
            NormalizedPath CheckWritableStorePath( string storePath )
            {
                NormalizedPath p = storePath != null ? new NormalizedPath( Path.GetFullPath( storePath ) ) : _defaultStorePath;
                if( p == Facade.DefaultStorePath ) throw new ArgumentException( $"Store path can not be the shared one ({Facade.DefaultStorePath}).", nameof( storePath ) );
                return p;
            }

            public bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey )
            {
                if( storePath == null ) throw new ArgumentNullException( nameof( storePath ) );
                if( remoteStoreUrl == null ) throw new ArgumentNullException( nameof( remoteStoreUrl ) );
                using( LocalStore store = LocalStore.OpenOrCreate( _monitor.Monitor, storePath ) )
                {
                    return store != null
                            && store.PushComponents( comp => true, new Uri( remoteStoreUrl, UriKind.Absolute ), apiKey );
                }
            }

            public NormalizedPath GetTestStorePath( string suffix = null, [CallerMemberName]string name = null )
            {
                return _localStoresFolder.AppendPart( name + suffix );
            }


            [Obsolete( "v10 or v11 version will support only Directory based store: use the GetTestStorePath() without store type." )]
            public NormalizedPath GetTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null )
            {
                return type == CKSetupStoreType.Zip
                        ? _localStoresFolder.AppendPart( name + suffix + ".zip" )
                        : _localStoresFolder.AppendPart( name + suffix );
            }

            public NormalizedPath GetCleanTestStorePath( string suffix = null, [CallerMemberName]string name = null )
            {
                var p = GetTestStorePath( suffix, name );
                if( Directory.Exists( p ) ) _monitor.CleanupFolder( p );
                return p;
            }

            [Obsolete( "v10 or v11 version will support only Directory based store: use the GetTestStorePath() without store type." )]
            public NormalizedPath GetCleanTestStorePath( CKSetupStoreType type, string suffix = null, [CallerMemberName]string name = null )
            {
                var p = GetTestStorePath( type, suffix, name );
                if( type == CKSetupStoreType.Zip ) File.Delete( p );
                else if( Directory.Exists( p ) ) _monitor.CleanupFolder( p );
                return p;
            }
        }

        internal CKSetupTestHelper( ITestHelperConfiguration config, IMonitorTestHelper monitor )
        {
            _driver = new Driver( config, monitor );
        }

        ICKSetupDriver ICKSetupTestHelperCore.CKSetup => _driver;

        /// <summary>
        /// Gets the <see cref="ICKSetupTestHelper"/> mixin interface.
        /// </summary>
        public static ICKSetupTestHelper TestHelper => TestHelperResolver.Default.Resolve<ICKSetupTestHelper>();

    }
}
