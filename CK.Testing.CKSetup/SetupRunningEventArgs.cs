using CK.Text;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Event argument for <see cref="ICKSetupDriver.SetupRunning"/> event.
    /// </summary>
    public class SetupRunningEventArgs : EventArgs
    {
        NormalizedPath _storePath;
        SetupConfiguration _configuration;

        /// <summary>
        /// Initializes a new <see cref="SetupRunningEventArgs"/>.
        /// </summary>
        /// <param name="config">The setup configuration.</param>
        /// <param name="storePath">The path of the store.</param>
        /// <param name="remoteStoreUrl">The uri of the remote store.</param>
        /// <param name="launchDebug">Whether the setup process should launch the debugger.</param>
        /// <param name="forceSetup">
        /// Whether the setup should be done, regardless of the signature files that may
        /// exist in the <see cref="SetupConfiguration.BinPaths"/> folders.
        /// </param>
        public SetupRunningEventArgs( SetupConfiguration config, NormalizedPath storePath, Uri remoteStoreUrl, bool launchDebug, bool forceSetup )
        {
            _configuration = config;
            _storePath = storePath;
            RemoteStoreUrl = remoteStoreUrl;
            LaunchDebug = launchDebug;
            ForceSetup = forceSetup;
        }

        /// <summary>
        /// Gets or sets whether the setup should be canceled.
        /// Obviously defaults to false.
        /// </summary>
        public bool Cancel { get; set; }

        /// <summary>
        /// Gets or sets a mutable Setup configuration. Must not be set to null.
        /// </summary>
        public SetupConfiguration Configuration
        {
            get => _configuration;
            set => _configuration = value ?? throw new ArgumentNullException();
        }

        /// <summary>
        /// Gets or sets the path of the store that will be used by <see cref="ICKSetupDriver.Run"/>.
        /// Must not be <see cref="NormalizedPath.IsEmptyPath"/>.
        /// </summary>
        public NormalizedPath StorePath
        {
            get => _storePath;
            set
            {
                if( value.IsEmptyPath ) throw new ArgumentNullException( nameof( StorePath ) );
                _storePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the url of the remote store that will be used by <see cref="ICKSetupDriver.Run"/>.
        /// Sets it to null to specify "none" store.
        /// </summary>
        public Uri RemoteStoreUrl { get; set; }

        /// <summary>
        /// Gets or sets whether the debugger should be launched by the runner process.
        /// </summary>
        public bool LaunchDebug { get; set; }

        /// <summary>
        /// Gets or sets whether the setup should be ran, regardless of the signature files that may
        /// exist in the <see cref="SetupConfiguration.BinPaths"/> folders.
        /// </summary>
        public bool ForceSetup { get; set; }
    }
}
