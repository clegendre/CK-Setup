using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// This helper gives access to a <see cref="ICKSetupDriver"/> that exposes
    /// the API to avoid cluttering the global test helper interface.
    /// </summary>
    public interface ICKSetupTestHelperCore
    {
        /// <summary>
        /// Gets the CKSetup driver interface.
        /// </summary>
        ICKSetupDriver CKSetup { get; }
    }
}
