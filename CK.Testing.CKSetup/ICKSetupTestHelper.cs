using System;

namespace CK.Testing
{
    /// <summary>
    /// Mixin of <see cref="IMonitorTestHelper"/> and <see cref="CKSetup.ICKSetupTestHelperCore"/>.
    /// </summary>
    public interface ICKSetupTestHelper : IMixinTestHelper, IMonitorTestHelper, CKSetup.ICKSetupTestHelperCore
    {
    }
}
