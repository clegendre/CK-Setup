using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Testing
{
    /// <summary>
    /// Defines the store type: can be a zip file or a directory.
    /// </summary>
    [Obsolete( "v10 or v10 or v10 or v11 version will support only Directory based store." )]
    public enum CKSetupStoreType
    {
        /// <summary>
        /// The store is a directory.
        /// </summary>
        Directory,

        /// <summary>
        /// The store is a zip file.
        /// </summary>
        Zip
    }
}
