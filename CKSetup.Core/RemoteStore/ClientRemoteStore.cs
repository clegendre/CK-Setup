using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using CKSetup.StreamStore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO.Compression;
using System.Net;
using System.Xml.Linq;
using System.Xml;

namespace CKSetup
{
    /// <summary>
    /// Uses a <see cref="HttpClient"/> to interact with a remote store.
    /// Uses <see cref="Task.GetAwaiter"/> to perform sync to HttpClient async calls.
    /// </summary>
    /// <remarks>
    /// See https://stackoverflow.com/questions/43382460/multiple-calls-to-static-httpclient-hangs-console-application and 
    /// https://stackoverflow.com/questions/9343594/how-to-call-asynchronous-method-from-synchronous-method-in-c.
    /// </remarks>
    public class ClientRemoteStore : IRemoteStore
    {
        /// <summary>
        /// Root path string is "/.cksetup/store".
        /// </summary>
        static public readonly string RootPathString = "/.cksetup/store";
        /// <summary>
        /// The header name of the API key: "X-API-Key"
        /// Required to push components.
        /// </summary>
        static public readonly string ApiKeyHeader = "X-API-Key";
        /// <summary>
        /// The header name for the Push sesion identifier: "X-SessionId";
        /// The session identifier is created by the Push request: pushing
        /// files require it.
        /// </summary>
        static public readonly string SessionIdHeader = "X-SessionId";

        /// <summary>
        /// The path to push a component.
        /// </summary>
        static public readonly string PushPath = "/push";

        /// <summary>
        /// The path to push a file.
        /// </summary>
        static public readonly string PushFilePath = "/push/f";

        /// <summary>
        /// The path to pull a component.
        /// </summary>
        static public readonly string PullPath = "/pull";

        /// <summary>
        /// The path to pull a file.
        /// </summary>
        static public readonly string PullFilePath = "/pull/f";

        readonly Uri _remoteStoreUrl;
        readonly string _remotePrefix;
        readonly string _pushApiKey;
        readonly Func<HttpClient> _httpClientProvider;
        readonly bool _ownHttpClient;
        HttpClient _httpClient;

        ClientRemoteStore( Uri remoteStoreUrl, string pushApiKey, Func<HttpClient> httpClientProvider = null )
        {
            if( remoteStoreUrl == null ) throw new ArgumentNullException( nameof( remoteStoreUrl ) );
            if( remoteStoreUrl.IsFile || remoteStoreUrl.IsUnc ) throw new ArgumentException( "Must be a http:// or https:// url.", nameof( remoteStoreUrl ) );
            _remoteStoreUrl = remoteStoreUrl;
            if( (_httpClientProvider = httpClientProvider) == null )
            {
                _httpClientProvider = () => new HttpClient();
                _ownHttpClient = true;
            }
            _remotePrefix = remoteStoreUrl + RootPathString.Substring( 1 );
            _pushApiKey = pushApiKey;
        }

        /// <summary>
        /// Internal class that implements <see cref="IRemoteStore"/> on a local store.
        /// This is used when a local or UNC path is provided as the url of the remote.
        /// </summary>
        class LocalAdapter : IRemoteStore
        {
            readonly LocalStore _store;

            public LocalAdapter( LocalStore s )
            {
                _store = s;
            }

            Uri IComponentFileDownloader.Url => _store.Remote.Url;

            void IDisposable.Dispose() => _store.Dispose();

            StoredStream IComponentFileDownloader.GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind kind )
                => _store.Remote.GetDownloadStream( monitor, file, kind );

            Stream IComponentImporter.OpenImportStream( IActivityMonitor monitor, ComponentMissingDescription missing )
                => _store.Remote.OpenImportStream( monitor, missing );

            PushComponentsResult IComponentPushTarget.PushComponents( IActivityMonitor monitor, Action<Stream> componentsWriter )
                => _store.Remote.PushComponents( monitor, componentsWriter );

            bool IComponentPushTarget.PushFile( IActivityMonitor monitor, string sessionId, SHA1Value sha1, Action<Stream> writer, CompressionKind kind )
                => _store.Remote.PushFile( monitor, sessionId, sha1, writer, kind );
        }

        /// <summary>
        /// Creates a <see cref="IRemoteStore"/> on a regular http:// or https:// url or
        /// on a 'file:' url (bound to a local store). Returns null on error or if the local store does not exist.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="localOrRemoteUrl">The url.</param>
        /// <param name="pushApiKey">
        /// Api key that may be required to push components to remote store.
        /// Does not apply to local stores.
        /// </param>
        /// <param name="httpClient">
        /// Optional shared http client.
        /// Applies only to Http(s) urls: by default a new one is created and will be disposed once the
        /// remote store is disposed.
        /// </param>
        /// <returns>The remote store or null on error or if the local store does not exist.</returns>
        public static IRemoteStore Create( IActivityMonitor monitor, Uri localOrRemoteUrl, string pushApiKey, Func<HttpClient> httpClient = null )
        {
            if( localOrRemoteUrl == null ) throw new ArgumentNullException( nameof( localOrRemoteUrl ) );
            using( monitor.OpenTrace( $"Creating client remote store on '{localOrRemoteUrl}'." ) )
            {
                if( localOrRemoteUrl.IsFile || localOrRemoteUrl.IsUnc )
                {
                    var store = LocalStore.Open( monitor, localOrRemoteUrl.LocalPath );
                    if( store == null ) monitor.CloseGroup( "Local store not found." );
                    return store != null ? new LocalAdapter( store ) : null;
                }
                else
                {
                    return new ClientRemoteStore( localOrRemoteUrl, pushApiKey, httpClient );
                }
            }
        }

        HttpClient GetHttpClient() => _httpClient ?? (_httpClient = _httpClientProvider());

        Uri IComponentFileDownloader.Url => _remoteStoreUrl;

        Stream IComponentImporter.OpenImportStream( IActivityMonitor monitor, ComponentMissingDescription missing )
        {
            string pullUrl = _remotePrefix + PullPath;
            try
            {
                using( var buffer = new MemoryStream() )
                {
                    using( var w = XmlWriter.Create( buffer, new XmlWriterSettings() { CloseOutput = false, Indent = false } ) )
                    {
                        missing.ToXml().WriteTo( w );
                    }
                    HttpResponseMessage r;
                    buffer.Position = 0;
                    using( var c = new StreamContent( buffer ) )
                    {
                        r = GetHttpClient().PostAsync( pullUrl, c ).GetAwaiter().GetResult();
                    }
                    return r.Content.ReadAsStreamAsync().GetAwaiter().GetResult();
                }
            }
            catch( Exception ex )
            {
                monitor.Error( "Call to remote store error: " + pullUrl, ex );
                return null;
            }
        }

        StoredStream IComponentFileDownloader.GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind kind )
        {
            var url = _remotePrefix + PullFilePath + '/' + file.ToString();
            return new StoredStream( CompressionKind.GZiped,
                                     GetHttpClient().GetAsync( url ).GetAwaiter().GetResult()
                                        .Content.ReadAsStreamAsync().GetAwaiter().GetResult() );
        }

        PushComponentsResult IComponentPushTarget.PushComponents( IActivityMonitor monitor, Action<Stream> componentsWriter )
        {
            string pushUrl = _remotePrefix + PushPath;
            try
            {
                using( var buffer = new MemoryStream() )
                {
                    componentsWriter( buffer );
                    buffer.Position = 0;
                    using( var c = new StreamContent( buffer ) )
                    {
                        c.Headers.Add( ApiKeyHeader, _pushApiKey );
                        using( HttpResponseMessage r = GetHttpClient().PostAsync( pushUrl, c ).GetAwaiter().GetResult() )
                        {
                            if( !r.IsSuccessStatusCode )
                            {
                                return new PushComponentsResult( $"Remote response Status: {r.StatusCode}.", null );
                            }
                            using( var responseStream = r.Content.ReadAsStreamAsync().GetAwaiter().GetResult() )
                            {
                                return new PushComponentsResult( new CKBinaryReader( responseStream ) );
                            }
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                monitor.Error( "Call to remote store error: " + pushUrl, ex );
                return new PushComponentsResult( ex.Message, null );
            }
        }

        bool IComponentPushTarget.PushFile( IActivityMonitor monitor, string sessionId, SHA1Value sha1, Action<Stream> writer, CompressionKind kind )
        {
            string url = null;
            try
            {
                bool success = true;
                using( var buffer = new MemoryStream() )
                {
                    if( kind == CompressionKind.None ) writer = StreamStoreExtension.GetCompressShell( writer );
                    writer( buffer );
                    buffer.Position = 0;
                    using( var c = new StreamContent( buffer ) )
                    {
                        c.Headers.Add( SessionIdHeader, sessionId );
                        url = _remotePrefix + PushFilePath + '/' + sha1;
                        using( HttpResponseMessage r = GetHttpClient().PostAsync( url, c ).GetAwaiter().GetResult() )
                        {
                            if( !r.IsSuccessStatusCode )
                            {
                                monitor.Error( $"Remote response Status: {r.StatusCode}." );
                                success = false;
                            }
                        }
                    }
                }
                return success;
            }
            catch( Exception ex )
            {
                monitor.Error( "Call to remote store error: " + url, ex );
                return false;
            }
        }

        /// <summary>
        /// Disposes the http client if it is owned.
        /// </summary>
        public void Dispose()
        {
            if( _ownHttpClient && _httpClient != null )
            {
                _httpClient.Dispose();
                _httpClient = null;
            }
        }


    }
}
