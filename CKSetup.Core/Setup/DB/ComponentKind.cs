using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Describes the 2 kind of components.
    /// </summary>
    public enum ComponentKind
    {
        /// <summary>
        /// Not applicable.
        /// </summary>
        None,

        /// <summary>
        /// The component is a Model.
        /// </summary>
        Model,

        /// <summary>
        /// The component is a setup dependency.
        /// </summary>
        SetupDependency
    }
}
