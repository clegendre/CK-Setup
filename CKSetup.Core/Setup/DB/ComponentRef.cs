using CK.Core;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Immutable struct that identifies a <see cref="Component"/>.
    /// </summary>
    public struct ComponentRef : IEquatable<ComponentRef>
    {
        readonly TargetFramework _targetFramework;
        readonly string _name;
        readonly SVersion _version;

        /// <summary>
        /// Initializes a new <see cref="ComponentRef"/>.
        /// </summary>
        /// <param name="name">Component name.</param>
        /// <param name="t">Target framework of the component.</param>
        /// <param name="v">
        /// Version of the component. It must be valid and should be based on a short form (see <see cref="CSVersion.IsLongForm"/>).
        /// If it happens to be a <see cref="CSVersion"/>, <see cref="CSVersion.ToNormalizedForm()"/> is called.
        /// </param>
        public ComponentRef( string name, TargetFramework t, SVersion v )
        {
            _targetFramework = t;
            _name = name;
            _version = v?.AsCSVersion?.ToNormalizedForm() ?? v;
            CheckValid();
        }

        /// <summary>
        /// Initializes a new <see cref="ComponentRef"/> from its xml representation.
        /// </summary>
        /// <param name="e">The xml element. Must not be null.</param>
        public ComponentRef( XElement e )
        {
            _targetFramework = e.AttributeEnum( DBXmlNames.TargetFramework, TargetFramework.None );
            _name = (string)e.AttributeRequired( DBXmlNames.Name );
            var vS = (string)e.Attribute( DBXmlNames.SVersion ) ?? (string)e.AttributeRequired( DBXmlNames.Version );
            var v = SVersion.TryParse( vS );
            _version = v.AsCSVersion?.ToNormalizedForm() ?? v;
            CheckValid();
        }

        void CheckValid()
        {
            if( _targetFramework == TargetFramework.None ) throw new ArgumentException( "Invalid TargetFramework." );
            if( string.IsNullOrWhiteSpace( _name ) ) throw new ArgumentException( "Invalid Name." );
            if( _version == null || !_version.IsValid ) throw new ArgumentException( $"Invalid Version '{_version}'." );
        }

        /// <summary>
        /// Gets the component's framework.
        /// </summary>
        public TargetFramework TargetFramework => _targetFramework;

        /// <summary>
        /// Gets the component's name.
        /// </summary>
        public string Name => _name;

        /// <summary>
        /// Returns a <see cref="ComponentRef"/> with the same name and version but a different framework.
        /// </summary>
        public ComponentRef WithTargetFramework( TargetFramework t ) => _targetFramework != t ? new ComponentRef( _name, t, _version ) : this;

        /// <summary>
        /// Gets the component's version.
        /// This version is normalized to the short form (<see cref="SVersion.ToNormalizedString()"/>).
        /// </summary>
        public SVersion Version => _version;

        /// <summary>
        /// Return the xml representation of this <see cref="ComponentRef"/>.
        /// </summary>
        /// <returns>The xml element.</returns>
        public XElement ToXml() => new XElement( DBXmlNames.Ref, XmlContent() );

        internal IEnumerable<XObject> XmlContent()
        {
            yield return new XAttribute( DBXmlNames.TargetFramework, _targetFramework );
            yield return new XAttribute( DBXmlNames.Name, _name );
            yield return new XAttribute( DBXmlNames.Version, _version.NormalizedText );
        }

        /// <summary>
        /// Gets the entry path prefix: "{Name}/{Version.NormalizedText}/{TargetFramework}/" (ends with a /).
        /// </summary>
        public string EntryPathPrefix => $"{Name}/{Version.NormalizedText}/{TargetFramework}/";

        /// <summary>
        /// Overridden to return the <see cref="EntryPathPrefix"/>.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => EntryPathPrefix;

        /// <summary>
        /// Implement value semantics.
        /// </summary>
        /// <param name="other">The other reference.</param>
        /// <returns>True when this references the same component as the other one.</returns>
        public bool Equals( ComponentRef other ) => _targetFramework == other._targetFramework && _name == other._name && _version == other._version;

        /// <summary>
        /// Overridden to implement a value semantics.
        /// </summary>
        /// <param name="obj">The other potential reference.</param>
        /// <returns>True if the other object is a reference to the same component.</returns>
        public override bool Equals( object obj ) => obj is ComponentRef ? Equals( (ComponentRef)obj ) : false;

        /// <summary>
        /// Returns the hash code based on <see cref="TargetFramework"/>, <see cref="Name"/> and <see cref="Version"/>.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode() => Util.Hash.Combine( (long)_targetFramework, _name, _version ).GetHashCode();
    }
}
