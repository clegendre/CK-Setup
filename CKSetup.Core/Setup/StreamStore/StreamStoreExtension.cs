using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup.StreamStore
{
    /// <summary>
    /// Extends <see cref="StoredStream"/> with useful extension methods.
    /// </summary>
    static public class StreamStoreExtension
    {
        /// <summary>
        /// Creates a new entry with an initial text content.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="text">Text (can not be null).</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <returns>The creation time in Utc. Can be used for optimistic concurrency check.</returns>
        static public DateTime CreateText( this IStreamStore @this, string fullName, string text, CompressionKind storageKind )
        {
            void writer( Stream w )
            {
                byte[] b = Encoding.UTF8.GetBytes( text );
                w.Write( b, 0, b.Length );
            }
            return @this.Create( fullName,
                                 storageKind == CompressionKind.GZiped ? GetCompressShell( writer ) : writer,
                                 storageKind );
        }

        /// <summary>
        /// Updates an entry, optionnaly allow creating it if it does not existsand optionnally
        /// handles optimistic concurrency: the only case where this method must return false
        /// is when <paramref name="checkLastWriteTimeUtc"/> is provided and do not match the current
        /// last write time of the entry.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="text">Text (can not be null).</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <param name="allowCreate">True to automatically creates the entry if it does not already exist.</param>
        /// <param name="checkLastWriteTimeUtc">Optional optimistic concurrency check.</param>
        /// <returns>The new last write time in Utc or <see cref="CK.Core.Util.UtcMaxValue"/> if optimistic concurrency check failed.</returns>
        static public DateTime UpdateText( this IStreamStore @this, string fullName, string text, CompressionKind storageKind, bool allowCreate = false, DateTime checkLastWriteTimeUtc = default( DateTime ) )
        {
            void writer( Stream w )
            {
                byte[] b = Encoding.UTF8.GetBytes( text );
                w.Write( b, 0, b.Length );
            }
            return @this.Update( fullName,
                                  storageKind == CompressionKind.GZiped ? GetCompressShell( writer ) : writer,
                                  storageKind,
                                  allowCreate,
                                  checkLastWriteTimeUtc );
        }

        /// <summary>
        /// Reads an existing resource previously written by <see cref="CreateText"/> or <see cref="UpdateText"/>
        /// or null if it does not exist.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <returns>The text and its last write time in Utc or null if not found.</returns>
        static public Tuple<string,DateTime> ReadText( this IStreamStore @this, string fullName )
        {
            LocalStoredStream s = OpenRead( @this, fullName, CompressionKind.None );
            if( s.Stream == null ) return null;
            using( var r = new StreamReader( s.Stream, Encoding.UTF8, false ) )
            {
                return Tuple.Create( r.ReadToEnd(), s.LastWriteTimeUtc );
            }
        }

        /// <summary>
        /// Tries to open an existing resource stream, uncompressing the data as necessary
        /// or letting it be compressed if the storage compression is the <paramref name="preferred"/> one.
        /// The <see cref="StoredStream.Stream"/> is null if it does not exist.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="preferred">Only None will always be honored. When GZip, it will be honored only if the actual stream is already gzipped.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <returns>An opened readable stream along with its compression kind and last write time (or a null <see cref="StoredStream.Stream"/> if it does not exist).</returns>
        static public LocalStoredStream OpenRead( this IStreamStore @this, string fullName, CompressionKind preferred )
        {
            LocalStoredStream s = @this.OpenRead( fullName );
            if( s.Stream == null ) return s;
            switch( s.Kind )
            {
                case CompressionKind.None:
                    return s;
                case CompressionKind.GZiped:
                    switch( preferred )
                    {
                        case CompressionKind.None: return new LocalStoredStream( preferred, new GZipStream( s.Stream, CompressionMode.Decompress, leaveOpen: false ), s.LastWriteTimeUtc );
                        case CompressionKind.GZiped: return s;
                    }
                    break;
            }
            throw new ArgumentException( $"Unknown {s.Kind} or {preferred}.", nameof( s.Kind ) );
        }


        /// <summary>
        /// Creates a new entry with an initial content from an input stream.
        /// </summary>
        /// <param name="this">This component storage.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="input"></param>
        /// <param name="inputKind">Specifies the content's stream compression.</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <returns>The creation time in Utc. Can be used for optimistic concurrency check.</returns>
        static public DateTime Create( this IStreamStore @this, string fullName, Stream input, CompressionKind inputKind, CompressionKind storageKind )
        {
            if( input == null ) throw new ArgumentNullException( nameof( input ) );
            switch( inputKind )
            {
                case CompressionKind.None:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            return @this.Create( fullName, w => input.CopyTo( w ), storageKind );
                        case CompressionKind.GZiped:
                            return @this.Create( fullName, GetCompressShell( w => input.CopyTo( w ) ), storageKind );
                    }
                    break;
                case CompressionKind.GZiped:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            using( var decompressor = new GZipStream( input, CompressionMode.Decompress, true ) )
                            {
                                return @this.Create( fullName, w => decompressor.CopyTo( w ), storageKind );
                            }
                        case CompressionKind.GZiped:
                            return @this.Create( fullName, w => input.CopyTo( w ), storageKind );
                    }
                    break;
            }
            throw new ArgumentException( $"Unknown {inputKind} or {storageKind}.", "kind" );
        }

        /// <summary>
        /// Updates an entry, optionnaly allow creating it if it does not exists and optionnally
        /// handles optimistic concurrency: the only case where this method must return false
        /// is when <paramref name="checkLastWriteTimeUtc"/> is provided and do not match the current
        /// last write time of the entry.
        /// </summary>
        /// <param name="this">This component storage.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="writer">The stream writer.</param>
        /// <param name="inputKind">Specifies the content's writer stream compression.</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <param name="allowCreate">True to automatically creates the entry if it does not already exist.</param>
        /// <param name="checkLastWriteTimeUtc">Optional optimistic concurrency check.</param>
        /// <returns>The new last write time in Utc or <see cref="CK.Core.Util.UtcMaxValue"/> if optimistic concurrency check failed.</returns>
        static public DateTime Update( this IStreamStore @this, string fullName, Action<Stream> writer, CompressionKind inputKind, CompressionKind storageKind, bool allowCreate, DateTime checkLastWriteTimeUtc = default( DateTime ) )
        {
            if( writer == null ) throw new ArgumentNullException( nameof( writer ) );
            switch( inputKind )
            {
                case CompressionKind.None:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            return @this.Update( fullName, writer, storageKind, allowCreate, checkLastWriteTimeUtc );
                        case CompressionKind.GZiped:
                            return @this.Update( fullName, GetCompressShell( writer ), storageKind, allowCreate, checkLastWriteTimeUtc );
                    }
                    break;
                case CompressionKind.GZiped:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            using( var buffer = new MemoryStream() )
                            {
                                writer( buffer );
                                buffer.Position = 0;
                                using( var decompressor = new GZipStream( buffer, CompressionMode.Decompress, true ) )
                                {
                                    return @this.Update( fullName, w => decompressor.CopyTo( w ), storageKind, allowCreate, checkLastWriteTimeUtc );
                                }
                            }
                        case CompressionKind.GZiped:
                            return @this.Update( fullName, writer, storageKind, allowCreate, checkLastWriteTimeUtc );
                    }
                    break;
            }
            throw new ArgumentException( $"Unknown {inputKind} or {storageKind}.", "kind" );
        }

        /// <summary>
        /// Creates a stream compressor wrapper action.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        /// <returns>The writer or an adapted writer.</returns>
        static public Action<Stream> GetCompressShell( Action<Stream> writer )
        {
            return w =>
            {
                using( var compressor = new GZipStream( w, CompressionLevel.Optimal, true ) )
                {
                    writer( compressor );
                    compressor.Flush();
                }
            };
        }

        /// <summary>
        /// Creates a stream compressor wrapper action.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        /// <returns>The writer or an adapted writer.</returns>
        static public Func<Stream,Task> GetCompressShellAsync( Func<Stream,Task> writer )
        {
            return async w =>
            {
                using( var compressor = new GZipStream( w, CompressionLevel.Optimal, true ) )
                {
                    await writer( compressor );
                    compressor.Flush();
                }
            };
        }

    }
}
