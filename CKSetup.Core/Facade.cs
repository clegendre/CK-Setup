using CK.Core;
using CK.Monitoring.InterProcess;
using CK.Text;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// This facade encapsulates the CKSetup run operations.
    /// </summary>
    static public class Facade
    {
        /// <summary>
        /// The default store is the directory Environment.SpecialFolder.LocalApplicationData/CKSetupStore
        /// (ie. '%UserProfile%/AppData/Local/CKSetupStore' folder).
        /// This is the default shared store that is user and machine specific.
        /// </summary>
        public static readonly string DefaultStorePath = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), "CKSetupStore" );

        /// <summary>
        /// The default remote store is https://cksetup.invenietis.net.
        /// </summary>
        public static readonly Uri DefaultStoreUrl = new Uri( "https://cksetup.invenietis.net" );

        /// <summary>
        /// Locates a store that must be used.
        /// This method never fails and ultimately defaults to <see cref="DefaultStorePath"/>.
        /// If a <paramref name="storePath"/> is provided, it will be used (it it does not already exist, a warning is emitted).
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <param name="storePath">A path to an existing store or to a store that will be created.</param>
        /// <param name="startPath">An optional path to look for a store up to the root.</param>
        /// <returns>True on success, false on error.</returns>
        public static string LocateStorePath( IActivityMonitor m, string storePath = null, string startPath = null )
        {
            var result = storePath;
            if( string.IsNullOrEmpty( result ) )
            {
                result = FindStorePathFrom( startPath );
                if( result == null )
                {
                    result = FindStorePathFrom( AppContext.BaseDirectory );
                }
                if( result == null )
                {
                    result = DefaultStorePath;
                }
            }
            else
            {
                result = Path.GetFullPath( result );
                if( !File.Exists( result ) && !Directory.Exists( result ) )
                {
                    m.Warn( $"The provided store '{result}' does not exist. It will be created." );
                }
            }
            return result;
        }

        static string FindStorePathFrom( string dir )
        {
            while( !string.IsNullOrEmpty( dir ) )
            {
                string test = Path.Combine( dir, "CKSetupStore.zip" );
                if( File.Exists( test ) ) return test;
                test = Path.Combine( dir, "CKSetupStore" );
                if( Directory.Exists( test ) ) return test;
                dir = Path.GetDirectoryName( dir );
            }
            return null;
        }

        /// <summary>
        /// Read multiple <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="paths">Folder paths to read.</param>
        /// <returns>The list of bin folders or null if an error occurred.</returns>
        public static List<BinFolder> ReadBinFolders( IActivityMonitor monitor, IEnumerable<string> paths )
        {
            var result = new List<BinFolder>();
            using( monitor.OpenDebug( "Discovering files." ) )
            {
                try
                {
                    foreach( var d in paths )
                    {
                        var f = BinFolder.ReadBinFolder( monitor, d );
                        if( f == null ) return null;
                        result.Add( f );
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                    return null;
                }
            }
            if( result.Count == 0 )
            {
                monitor.Error( "No valid runtime files found." );
                return null;
            }
            return result;
        }

        /// <summary>
        /// Captures <see cref="EnsurePublished(IActivityMonitor, string, bool)"/> result.
        /// </summary>
        public readonly struct EnsurePublishResult
        {
            /// <summary>
            /// Gets whether the publish succeeds.
            /// </summary>
            public bool Success { get; }

            /// <summary>
            /// Gets the published path if it is a netcore folder. Empty otherwise.
            /// </summary>
            public NormalizedPath PublishedPath { get; }

            /// <summary>
            /// Gets the final path: <see cref="PublishedPath"/> for netcore folder or the original folder
            /// for .Net framework or netstandard folder.
            /// </summary>
            public NormalizedPath FinalPath { get; }

            internal EnsurePublishResult( bool success, NormalizedPath publishedPath, NormalizedPath path )
            {
                Success = success;
                PublishedPath = publishedPath;
                FinalPath = publishedPath.IsEmptyPath ? path : publishedPath;
            }

        }

        /// <summary>
        /// Runs "dotnet publish" on an output bin folder if it needs to be published.
        /// The path can end with "/publish" or be the path above (typically "bin/Debug/netcoreapp2.1").
        /// <para>
        /// </para>
        /// The configuration (Debug/Release) and target framework (netcoreapp2.1 for instance) are extracted
        /// from the path. When called on .Net framework (like 'net461' folders) or on net standard
        /// targets (like 'netstandard2.0'), nothing is done, a warning is emitted and an empty path is returned
        /// without any error.
        /// <para>
        /// If /publish folder exists and contains the project file name (.exe or .dll) with a LastWriteTime equal
        /// or greater than the one of the file in <paramref name="pathToFramework"/>, publish is skipped
        /// unless <paramref name="forcePublish"/> is true.
        /// </para>
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="pathToFramework">Path to the framework (typically ends with "bin/Debug/netcoreapp2.1") or to the "/publish".</param>
        /// <param name="forcePublish">
        /// True to force the publication in .net core even if the "publish/" folder is up to date.
        /// </param>
        /// <returns>A result object.</returns>
        static public EnsurePublishResult EnsurePublished( IActivityMonitor monitor, string pathToFramework, bool forcePublish = false )
        {
            using( monitor.OpenInfo( $"Ensure '{pathToFramework}' is published (forcePublish={forcePublish})." ) )
            {
                NormalizedPath publishPath = new NormalizedPath();
                try
                {
                    NormalizedPath path = Path.GetFullPath( pathToFramework );
                    if( IsPublishedRequired( monitor, ref path, forcePublish, out publishPath ) )
                    {
                        Debug.Assert( publishPath.LastPart == "publish" );
                        // publishPath is: project/bin/Debug/netcoreapp2.1/publish
                        var framework = publishPath.Parts[publishPath.Parts.Count - 2];
                        var configuration = publishPath.Parts[publishPath.Parts.Count - 3];
                        var projectPath = publishPath.RemoveLastPart( 4 );
                        var projectName = projectPath.LastPart;
                        var pI = new ProcessStartInfo()
                        {
                            WorkingDirectory = projectPath,
                            FileName = "dotnet",
                            // The NoBuild option is available since 2.1: https://github.com/dotnet/cli/issues/5331
                            Arguments = $"publish -c {configuration} -f {framework} --no-build",
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            RedirectStandardInput = true,
                            UseShellExecute = false,
                            CreateNoWindow = true
                        };
                        using( monitor.OpenInfo( $"Publishing {projectName}: dotnet {pI.Arguments}" ) )
                        using( Process cmdProcess = new Process() )
                        {
                            StringBuilder bErr = new StringBuilder();
                            cmdProcess.StartInfo = pI;
                            cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) bErr.Append( "<StdErr> " ).AppendLine( e.Data ); };
                            cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) monitor.Info( e.Data ); };
                            cmdProcess.Start();
                            cmdProcess.BeginErrorReadLine();
                            cmdProcess.BeginOutputReadLine();
                            cmdProcess.WaitForExit();
                            if( bErr.Length > 0 )
                            {
                                monitor.Error( bErr.ToString() );
                            }
                            if( cmdProcess.ExitCode != 0 )
                            {
                                monitor.Error( $"Process returned ExitCode {cmdProcess.ExitCode}." );
                                return new EnsurePublishResult( false, publishPath, path );
                            }
                        }
                    }
                    else monitor.CloseGroup( "Skipped." );
                    return new EnsurePublishResult( true, publishPath, path );
                }
                catch( Exception ex )
                {
                    monitor.Error( ex );
                    return new EnsurePublishResult( false, publishPath, new NormalizedPath() );
                }
            }
        }

        static bool IsPublishedRequired( IActivityMonitor monitor, ref NormalizedPath path, bool forcePublish, out NormalizedPath publishPath )
        {
            if( path.LastPart == "publish" )
            {
                publishPath = path;
                path = publishPath.RemoveLastPart();
            }
            else publishPath = path.AppendPart( "publish" );

            var framework = path.LastPart;
            if( !framework.StartsWith( "netcoreapp", StringComparison.OrdinalIgnoreCase ) )
            {
                publishPath = new NormalizedPath();
                monitor.Warn( "Not a /netcoreapp path." );
                return false;
            }
            monitor.Info( $"Parent folder is '{framework}': as a 'netcoreapp' folder it can be published." );
            if( forcePublish )
            {
                monitor.Trace( "Forcing publication." );
                return true;
            }
            // If there is no publish folder, we need to publish.
            if( !Directory.Exists( publishPath ) )
            {
                monitor.Trace( "No /publish directory." );
                return true;
            }
            var projectName = path.Parts[path.Parts.Count - 4];

            // If we don't find a .dll or .exe with the projectName, in doubt, we need to publish.
            var dllOrExeName = path.AppendPart( projectName );
            string source = dllOrExeName + ".dll";
            if( !File.Exists( source ) )
            {
                source = dllOrExeName + ".exe";
                if( !File.Exists( source ) )
                {
                    monitor.Warn( $"No {projectName} dll or exe found in base path '{path}'." );
                    return true;
                }
            }
            var pubTargetName = Path.GetFileName( source );
            var pubName = publishPath.AppendPart( pubTargetName );
            if( !File.Exists( pubName ) )
            {
                monitor.Trace( $"Target {pubTargetName} not found in /publish." );
                return true;
            }
            DateTime sourceTime = File.GetLastWriteTimeUtc( source );
            DateTime pubTime = File.GetLastWriteTimeUtc( pubName );
            bool older = pubTime < sourceTime;
            if( older )
            {
                monitor.Trace( $"{pubTargetName} is newer than publish/{pubTargetName}." );
                return true;
            }
            return false;
        }


        /// <summary>
        /// Main entry point to CKSetup run.
        /// The <paramref name="config"/> object is internally cloned before the run: it is not altered by the run.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="store">The opened, valid, local store.</param>
        /// <param name="config">The setup configuration.</param>
        /// <param name="missingImporter">Optional component importer. Can be null.</param>
        /// <param name="launchDebug">True to call <see cref="Debugger.Launch"/> at the very start of the runner process.</param>
        /// <param name="forceSetup">True to setup even if <see cref="SetupConfiguration.UseSignatureFiles"/> is true and the signature files match.</param>
        /// <returns>The <see cref="CKSetupRunResult"/> enum.</returns>
        public static CKSetupRunResult DoRun(
            IActivityMonitor monitor,
            LocalStore store,
            SetupConfiguration config,
            IComponentImporter missingImporter,
            bool launchDebug,
            bool forceSetup )
        {
            if( config == null ) throw new ArgumentNullException( nameof( config ) );

            config = config.Clone();
            using( monitor.OpenInfo( $"Running Setup." ) )
            {
                SetupContext ctx;
                BufferedFileMonitorClient fileClient;
                // Ensures that the level is at least Monitor while creating the SetupContext.
                // The Warn when the signature match is necessarily emitted.
                using( monitor.TemporarilySetMinimalFilter( monitor.ActualFilter.Combine( LogFilter.Monitor ) ) )
                {
                    fileClient = config.WorkingDirectoryHasLogs ? BufferedFileMonitorClient.RegisterClient( monitor ) : null;
                    monitor.Info( GetCKSetupCoreVersion() );
                    ctx = SetupContext.Create( monitor, config, forceSetup, fileClient );
                    if( !ctx.Success ) return CKSetupRunResult.Failed;
                    if( ctx.UpToDate )
                    {
                        monitor.Warn( "Signature match! System is up to date. Creation of a (useless) WorkingDirectory is skipped." );
                        return CKSetupRunResult.UpToDate;
                    }
                }
                // Applies the LogLevel from the configuration to the monitor itself. The change is scoped to this opened group.
                var level = monitor.ActualFilter.Combine( config.LogLevel );
                if( level != monitor.ActualFilter )
                {
                    monitor.UnfilteredLog( null, LogLevel.Info, $"LogLevel changed from {monitor.ActualFilter} to {level}.", monitor.NextLogTime(), null );
                    monitor.MinimalFilter = level;
                }
                var r = ActualRun( ctx );
                if( fileClient != null )
                {
                    fileClient.Dispose();
                    monitor.Output.UnregisterClient( fileClient );
                }
                monitor.CloseGroup( r );
                return r;
            }

            CKSetupRunResult ActualRun( SetupContext ctx )
            {
                try
                {
                    Debug.Assert( Directory.Exists( ctx.FinalWorkingDirectory ) );
                    var dependencies = config.Dependencies;

                    // If no explicit dependencies have been set on CKSetup.Runner...
                    if( !dependencies.Any( d => d.UseName == "CKSetup.Runner" ) )
                    {
                        var thisAssembly = (AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                        var thisVersion = new InformationalVersion( thisAssembly?.InformationalVersion );
                        // ...use the ZeroVersion when compiling uncomitted work
                        // ...or this CKSetup.Core version.
                        SVersion runnerVersion = thisVersion.Version ?? SVersion.ZeroVersion;
                        dependencies.Add( new SetupDependency( "CKSetup.Runner", runnerVersion ) );
                    }

                    using( monitor.OpenInfo( $"Copying {ctx.DedupFiles.Count} files from bin folders." ) )
                    {
                        var dedupFolders = new HashSet<NormalizedPath>( ctx.DedupFiles.Keys.Select( p => new NormalizedPath( p ).RemoveLastPart() ) );
                        foreach( var requiredFolder in dedupFolders )
                        {
                            var targetPath = ctx.FinalWorkingDirectory.Combine( requiredFolder );
                            if( !Directory.Exists( targetPath ) )
                            {
                                monitor.Debug( $"Creating '{targetPath}' directory." );
                                Directory.CreateDirectory( targetPath );
                            }
                        }
                        foreach( var f in ctx.DedupFiles.Values )
                        {
                            string targetPath = ctx.FinalWorkingDirectory.Combine( f.LocalFileName );
                            File.Copy( f.FullPath, targetPath );
                        }
                    }

                    bool RuntimeFileSkipper( IActivityMonitor m, ComponentFile c, NormalizedPath targetPath )
                    {
                        bool alreadyHere = File.Exists( targetPath );
                        if( alreadyHere )
                        {
                            m.Debug( $"File {c.Name} already exists." );
                        }
                        return !alreadyHere;
                    }

                    var extractResult = store.ExtractRuntimeDependencies(
                                            ctx.FinalWorkingDirectory,
                                            ctx.BinFolders,
                                            missingImporter,
                                            dependencies,
                                            config.PreferredTargetRuntimes,
                                            RuntimeFileSkipper );
                    if( !extractResult.Success )
                    {
                        return CKSetupRunResult.Failed;
                    }
                    using( monitor.OpenInfo( $"Generating CKSetup.Runner.Config.xml file." ) )
                    {
                        XElement AddCategorizedAssemblies( XElement parent, IEnumerable<BinFileAssemblyInfo> assemblies )
                        {
                            XElement ToXml( XName name, BinFileAssemblyInfo info )
                            {
                                var e = new XElement( name );
                                e.Value = info.Name.Name;
                                var v = info.InfoVersion?.Version;
                                if( v != null && v.IsValid ) e.Add( new XAttribute( SetupConfiguration.xSVersion, v.ToNormalizedString() ) );
                                return e;
                            }
                            var models = assemblies.Where( a => a.ComponentKind == ComponentKind.Model );
                            var setupDependencies = assemblies.Where( a => a.ComponentKind == ComponentKind.SetupDependency );
                            var modelDependents = assemblies.Where( a => a.ComponentKind == ComponentKind.None && (a.IsModelDependent || a.LocalDependencies.Any( dep => dep.IsModelDependentSource )) );
                            parent.Add( models.Select( a => ToXml( SetupConfiguration.xModel, a ) ) );
                            parent.Add( setupDependencies.Select( a => ToXml( SetupConfiguration.xDependency, a ) ) );
                            parent.Add( modelDependents.Select( a => ToXml( SetupConfiguration.xModelDependent, a ) ) );
                            return parent;
                        }

                        // Configuration has been cloned.
                        var binPathsComment = new XComment( "BinPaths elements: 'Path' attribute is the configured path, 'BinPath' is the normalized path that may be combined with 'BasePath' and changed to '/publish' sub folder if needed." );
                        if( config.LogLevel == LogFilter.Undefined ) config.LogLevel = monitor.MinimalFilter;
                        XElement root = config.Configuration;
                        if( config.SharedConfigurationMode )
                        {
                            root.Add( new XElement( SetupConfiguration.xRunSignature, ctx.CurrentSignature.ToString() ) );
                            root.Element( SetupConfiguration.xBinPaths ).AddBeforeSelf( binPathsComment );
                            foreach( BinFolder f in ctx.BinFolders )
                            {
                                var binPath = config.BinPathElements.Single( e => e.Attribute( SetupConfiguration.xBinPath ).Value == f.BinPath );
                                var assemblies = binPath.Element( SetupConfiguration.xAssemblies );
                                if( assemblies == null ) binPath.Add( assemblies = new XElement( SetupConfiguration.xAssemblies ) );
                                AddCategorizedAssemblies( assemblies, f.Assemblies );
                            }
                        }
                        else
                        {
                            root.Add( new XAttribute( SetupConfiguration.xEngine, config.EngineAssemblyQualifiedName ),
                                      new XElement( SetupConfiguration.xCKSetupName, config.CKSetupName ),
                                      new XElement( SetupConfiguration.xBasePath, ctx.BasePath ),
                                      new XElement( SetupConfiguration.xRunSignature, ctx.CurrentSignature.ToString() ),
                                      new XElement( SetupConfiguration.xLogLevel, config.LogLevel.ToString() ),
                                      binPathsComment,
                                      new XElement( SetupConfiguration.xBinPaths,
                                              ctx.BinFolders.Select( f => AddCategorizedAssemblies(
                                                                          // We clone the BinPath XElement to get the non-normalized 'Path' attribute. 
                                                                          new XElement( config.BinPathElements.Single( e => e.Attribute( SetupConfiguration.xBinPath ).Value == f.BinPath ) ),
                                                                          f.Assemblies ) ) ) );
                        }
                        monitor.Info( root.ToString() );
                        new XDocument( root ).Save( Path.Combine( ctx.FinalWorkingDirectory, "CKSetup.Runner.Config.xml" ) );
                    }
                    if( RunSetupRunner( monitor, ctx.FinalWorkingDirectory, launchDebug ) )
                    {
                        UpdateFileSignatures( monitor, ctx.BinFolders, ctx.CurrentSignature );
                        return CKSetupRunResult.Succeed;
                    }
                    // On failure, we set the SHA1 to the Zero.
                    UpdateFileSignatures( monitor, ctx.BinFolders, SHA1Value.ZeroSHA1 );
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                finally
                {
                    if( ctx.FinalWorkingDirectory.StartsWith( Path.GetTempPath() ) )
                    {
                        SafeDeleteFolder( monitor, ctx.FinalWorkingDirectory );
                    }
                    else monitor.Trace( $"Leaving Working Directory '{ctx.FinalWorkingDirectory}' as-is." );
                }
                return CKSetupRunResult.Failed;
            }
        }

        static string GetCKSetupCoreVersion()
        {
            var a = (AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute( typeof( Facade ).Assembly, typeof( AssemblyInformationalVersionAttribute ) );
            var thisFramework = Assembly.GetExecutingAssembly().CustomAttributes
               .Where( x => x.AttributeType.FullName == "System.Runtime.Versioning.TargetFrameworkAttribute" )
               .Select( x => x.ConstructorArguments )
               .Where( p => p.Count > 0 )
               .Select( p => p[0].Value as string )
               .FirstOrDefault();
            return "CKSetup.Core " + (thisFramework ?? "<no TargetFrameworkAttribute>") + " - " + (a != null ? a.InformationalVersion : "<no version>");
        }

        static void UpdateFileSignatures( IActivityMonitor monitor, IReadOnlyList<BinFolder> binFolders, SHA1Value currentSignature )
        {
            bool delete = currentSignature == SHA1Value.ZeroSHA1;
            using( monitor.OpenTrace( delete ? "Deleting signature files." : "Updating signature files." ) )
            {
                foreach( var f in binFolders )
                {
                    try
                    {
                        var fSign = Path.Combine( f.BinPath, SetupContext.SignatureFileName );
                        if( currentSignature == SHA1Value.ZeroSHA1 )
                        {
                            if( File.Exists( fSign ) ) File.Delete( fSign );
                        }
                        else
                        {
                            File.WriteAllText( fSign, currentSignature.ToString() );
                        }
                    }
                    catch( Exception ex )
                    {
                        monitor.Warn( ex );
                    }
                }
            }
        }

        internal static void SafeDeleteFolder( IActivityMonitor monitor, string dir )
        {
            using( monitor.OpenInfo( $"Deleting folder: {dir}" ) )
            {
                int retryCount = 0;
                for(; ; )
                {
                    try
                    {
                        Directory.Delete( dir, true );
                        break;
                    }
                    catch( Exception ex )
                    {
                        if( ++retryCount < 4 )
                        {
                            monitor.Warn( $"Error while deleting folder. Retrying (count = {retryCount}).", ex );
                            System.Threading.Thread.Sleep( 200 * retryCount );
                        }
                        else
                        {
                            monitor.Error( $"Unable to delete folder '{dir}'.", ex );
                            break;
                        }
                    }
                }
            }
        }

        static bool RunSetupRunner(
            IActivityMonitor m,
            string workingDir,
            bool launchDebug )
        {
            using( m.OpenInfo( "Launching CKSetup.Runner process." ) )
            {
                string exe = Path.Combine( workingDir, "CKSetup.Runner.exe" );
                string dll = Path.Combine( workingDir, "CKSetup.Runner.dll" );
                string fileName, arguments;
                if( !File.Exists( exe ) )
                {
                    if( !File.Exists( dll ) )
                    {
                        m.Error( "Unable to find CKSetup.Runner in folder." );
                        return false;
                    }
                    fileName = "dotnet";

                    #region Merging all deps.json into CKSetup.Runner.deps.json
                    {
                        arguments = "CKSetup.Runner.dll merge-deps";
                        if( launchDebug ) arguments += " /launchDebug";
                        if( !RunCKSetupRunnerProcess( m, workingDir, fileName, arguments ) )
                        {
                            return false;
                        }

                        string depsFile = Path.Combine( workingDir, "CKSetup.Runner.deps.json" );
                        string depsBackup = depsFile + ".original";
                        File.Replace( depsFile + ".merged", depsFile, depsBackup );

                        string rcFile = Path.Combine( workingDir, "CKSetup.Runner.runtimeconfig.json" );
                        string rcMerged = rcFile + ".merged";
                        // RuntimeConfig swap is not supported by older CKSetup.Runners below 10.1.1--0011-develop
                        if( File.Exists( rcMerged ) )
                        {
                            string rcBackup = rcFile + ".original";
                            File.Replace( rcMerged, rcFile, rcBackup );
                        }
                    }
                    #endregion
                    arguments = "CKSetup.Runner.dll ";
                }
                else
                {
                    fileName = exe;
                    arguments = String.Empty;
                }
                if( launchDebug ) arguments += "/launchDebug ";
                return RunCKSetupRunnerProcess( m, workingDir, fileName, arguments );
            }
        }

        static bool RunCKSetupRunnerProcess(
                IActivityMonitor m,
                string workingDir,
                string fileName,
                string arguments )
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDir,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = fileName,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
            };
            using( var logReceiver = SimpleLogPipeReceiver.Start( m, true ) )
            {
                cmdStartInfo.Arguments = arguments;
                cmdStartInfo.Arguments += " /silent /logPipe:" + logReceiver.PipeName;
                using( m.OpenTrace( $"{fileName} {cmdStartInfo.Arguments}" ) )
                using( Process cmdProcess = new Process() )
                {
                    StringBuilder conOut = new StringBuilder();
                    cmdProcess.StartInfo = cmdStartInfo;
                    cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) conOut.Append( "<StdErr> " ).AppendLine( e.Data ); };
                    cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) conOut.Append( "<StdOut> " ).AppendLine( e.Data ); };
                    cmdProcess.Start();
                    cmdProcess.BeginErrorReadLine();
                    cmdProcess.BeginOutputReadLine();
                    cmdProcess.WaitForExit();

                    if( conOut.Length > 0 ) m.Info( conOut.ToString() );
                    var endLogStatus = logReceiver.WaitEnd( cmdProcess.ExitCode != 0 );
                    if( endLogStatus != LogReceiverEndStatus.Normal )
                    {
                        m.Warn( $"Pipe log channel abnormal end status: {endLogStatus}." );
                    }
                    if( cmdProcess.ExitCode != 0 )
                    {
                        const string rawLogFileName = "CKSetup.Runner.RawLogs.txt";
                        using( m.OpenError( $"Process returned ExitCode {cmdProcess.ExitCode}. {rawLogFileName} file content:" ) )
                        {
                            var rawLog = Path.Combine( workingDir, rawLogFileName );
                            if( File.Exists( rawLog ) )
                            {
                                m.Debug( File.ReadAllText( rawLog ) );
                            }
                            else m.CloseGroup( $"No {rawLogFileName} file." );
                        }
                        return false;
                    }
                    return true;
                }
            }
        }

    }
}
