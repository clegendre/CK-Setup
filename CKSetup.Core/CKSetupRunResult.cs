using System;
using System.Collections.Generic;
using System.Text;

namespace CKSetup
{
    /// <summary>
    /// Captures the result of <see cref="Facade.DoRun"/> method.
    /// </summary>
    public enum CKSetupRunResult
    {
        /// <summary>
        /// Not applicable. Run has not been executed.
        /// </summary>
        None,

        /// <summary>
        /// The run has succeed.
        /// </summary>
        Succeed,

        /// <summary>
        /// The run has been skipped because <see cref="SetupConfiguration.UseSignatureFiles"/> was true
        /// and the signature files matched the configuration and folders content signature.
        /// </summary>
        UpToDate,

        /// <summary>
        /// The run failed.
        /// </summary>
        Failed
    }
}
