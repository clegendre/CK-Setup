using CK.Core;
using CK.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Captures informations related to the setup initialization.
    /// This class is currently internal but may be published if needed.
    /// </summary>
    class SetupContext
    {
        /// <summary>
        /// The signature file name that should contain the <see cref="CurrentSignature"/>.
        /// </summary>
        public static readonly string SignatureFileName = "CKSetup.signature.txt";

        SetupContext(
            bool success,
            SetupConfiguration config,
            IReadOnlyList<BinFolder> folders,
            IReadOnlyDictionary<string,BinFileInfo> files,
            SHA1Value currentSignature,
            NormalizedPath workingDir,
            NormalizedPath basePath )
        {
            Success = success;
            Configuration = config;
            BinFolders = folders;
            DedupFiles = files;
            CurrentSignature = currentSignature;
            FinalWorkingDirectory = workingDir;
            BasePath = basePath;
        }

        SetupContext( SetupConfiguration config )
            : this( false, config, null, null, SHA1Value.ZeroSHA1, null, null )
        {
        }

        /// <summary>
        /// Gets the configuration.
        /// This is a mutable object but it should no be modified once a <see cref="SetupContext"/> is built.
        /// </summary>
        public SetupConfiguration Configuration { get; }

        /// <summary>
        /// Gets the configuration base path.
        /// When <see cref="SetupConfiguration.BasePath"/> is empty, the current directory is used.
        /// </summary>
        public NormalizedPath BasePath { get; }

        /// <summary>
        /// Gets whether this context has been successfully created.
        /// </summary>
        public bool Success { get; }

        /// <summary>
        /// Gets whether signatures files have been checked and system is up to date:
        /// the <see cref="FinalWorkingDirectory"/> is null since no setup should be done.
        /// </summary>
        public bool UpToDate => Success && FinalWorkingDirectory == null;

        /// <summary>
        /// Gets the current signature based on the <see cref="Configuration"/> and the content of
        /// the <see cref="SetupConfiguration.BinPaths"/>.
        /// It is <see cref="SHA1Value.ZeroSHA1"/> if <see cref="Success"/> is false.
        /// </summary>
        public SHA1Value CurrentSignature { get; }

        /// <summary>
        /// Gets the <see cref="SetupConfiguration.BinPaths"/> as <see cref="BinFolder"/>.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyList<BinFolder> BinFolders { get; }

        /// <summary>
        /// Gets all the files indexed by their local file names in the different bin paths.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyDictionary<string, BinFileInfo> DedupFiles { get; }

        /// <summary>
        /// Gets the working directory that must be used.
        /// It is either a sub directory of <see cref="Path.GetTempPath()"/> or the
        /// explicit <see cref="SetupConfiguration.WorkingDirectory"/>.
        /// This is null if <see cref="Success"/> is false or <see cref="UpToDate"/> is true.
        /// </summary>
        public NormalizedPath FinalWorkingDirectory { get; }

        /// <summary>
        /// Creates a <see cref="SetupContext"/> from a <see cref="SetupConfiguration"/>.
        /// The configuration can be altered by this initialization.
        /// </summary>
        /// <param name="monitor">The monitor to use. Must not be null.</param>
        /// <param name="config">The configuration. Must not be null.</param>
        /// <param name="forceSetup">True to ignore <see cref="SetupConfiguration.UseSignatureFiles"/> configuration.</param>
        /// <param name="fileClient">Optional <see cref="BufferedFileMonitorClient"/> for which a log file in WorkingDirectory logs should be created.</param>
        /// <returns>A setup context (potentially invalid, see <see cref="Success"/>).</returns>
        public static SetupContext Create( IActivityMonitor monitor, SetupConfiguration config, bool forceSetup, BufferedFileMonitorClient fileClient = null )
        {
            if( monitor == null ) throw new ArgumentNullException( nameof( monitor ) );
            if( config == null ) throw new ArgumentNullException( nameof( config ) );

            bool checkSignature = !forceSetup && config.UseSignatureFiles;
            using( monitor.OpenTrace( $"Initializing SetupContext{(checkSignature ? String.Empty : " (ignoring signatures)")}." ) )
            {
                monitor.Info( config.ToXml().ToString() );
                try
                {
                    NormalizedPath basePath = config.BasePath;
                    if( basePath.IsEmptyPath )
                    {
                        basePath = Environment.CurrentDirectory;
                        monitor.Info( $"Configuration BasePath is empty: using current directory '{basePath}'." );
                    }
                    using( monitor.OpenInfo( "Ensures that all 'Path' attributes are published (creating normalized 'BinPath' attributes)." ) )
                    {
                        foreach( var binPath in config.BinPathElements )
                        {
                            var p = (string)binPath.Attribute( SetupConfiguration.xPath ) ?? String.Empty;
                            var nP = Path.IsPathRooted( p ) ? p : Path.Combine( basePath, p );
                            var r = Facade.EnsurePublished( monitor, nP );
                            if( !r.Success ) return new SetupContext( config );
                            binPath.SetAttributeValue( SetupConfiguration.xBinPath, r.FinalPath );
                        }
                    }
                    IReadOnlyDictionary<string, BinFileInfo> dedupFiles;
                    IReadOnlyList<BinFolder> folders = ReadBinFolders( monitor, config.FinalBinPaths, out dedupFiles );
                    if( folders != null )
                    {
                        SHA1Value currentSignature = ComputeCurrentSignature( config, dedupFiles );
                        if( checkSignature
                            && currentSignature == ReadFileSignatures( monitor, folders ) )
                        {
                            return new SetupContext( true, config, folders, dedupFiles, currentSignature, null, basePath );
                        }
                        var workingDir = GetWorkingDirectory( monitor, config.WorkingDirectory, config.CKSetupName, folders, basePath, config.WorkingDirectoryKeepOnlyCount );
                        if( !workingDir.IsEmptyPath )
                        {
                            if( fileClient != null )
                            {
                                fileClient.SetFile( monitor, workingDir.AppendPart( "CKSetup.log" ) );
                            }
                            if( checkSignature ) monitor.CloseGroup( "Signature does not match. Setup is required." );
                            return new SetupContext( true, config, folders, dedupFiles, currentSignature, workingDir, basePath );
                        }
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                monitor.CloseGroup( "Invalid SetupContext." );
                return new SetupContext( config );
            }
        }

        static SHA1Value ReadFileSignatures( IActivityMonitor monitor, IReadOnlyList<BinFolder> folders )
        {
            string current = null;
            foreach( var f in folders )
            {
                string fSign = Path.Combine( f.BinPath, SignatureFileName );
                if( !File.Exists( fSign ) )
                {
                    monitor.Info( $"Missing signature file '{fSign}'." );
                    return SHA1Value.ZeroSHA1;
                }
                var content = File.ReadAllText( fSign );
                if( current == null ) current = content;
                else if( current != content )
                {
                    monitor.Info( $"Signature mismatch between folders: {fSign} is not the same as the one in {folders[0].BinPath}." );
                    return SHA1Value.ZeroSHA1;
                }
            }
            if( SHA1Value.TryParse( current, 0, out var result ) )
            {
                monitor.Info( $"Found current signature among {folders.Count} folder(s): {result}." );
            }
            else
            {
                monitor.Warn( $"Unable to parse SHA1 signature: {current}." );
            }
            return result;
        }

        static SHA1Value ComputeCurrentSignature( SetupConfiguration config, IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            using( SHA1Stream c = new SHA1Stream() )
            using( BinaryWriter cW = new BinaryWriter( c ) )
            {
                cW.Write( config.ToXml( forSignature: true ).ToString( SaveOptions.DisableFormatting ) );
                // Sort them to be hash code (non)ordering independent.
                var content = dedupFiles.Values.Select( f => f.ContentSHA1 ).ToArray();
                Array.Sort( content );
                foreach( var s in content ) s.Write( cW );
                return c.GetFinalResult();
            }
        }

        static IReadOnlyList<BinFolder> ReadBinFolders(
                    IActivityMonitor monitor,
                    IEnumerable<string> binPaths,
                    out IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            dedupFiles = null;
            using( monitor.OpenInfo( "Reading binary folders." ) )
            {
                var folders = binPaths.Select( p => BinFolder.ReadBinFolder( monitor, p ) ).ToList();
                if( folders.Any( f => f == null ) ) return null;
                if( folders.Count == 0 )
                {
                    monitor.Error( $"No BinPath provided. At least one is required." );
                    return null;
                }
                using( folders.Count > 1 ? monitor.OpenInfo( "Multiple Bin paths: Checking that common files are the same." ) : null )
                {
                    bool conflict = false;
                    Dictionary<string, BinFileInfo> byName = new Dictionary<string, BinFileInfo>();
                    foreach( var folder in folders )
                    {
                        foreach( var file in folder.Files )
                        {
                            if( byName.TryGetValue( file.LocalFileName, out var exists ) )
                            {
                                if( exists.ContentSHA1 != file.ContentSHA1 )
                                {
                                    conflict = true;
                                    using( monitor.OpenError( $"File mismatch: {file.LocalFileName}" ) )
                                    {
                                        monitor.Info( $"{exists.BinFolder.BinPath} has {exists.ToString()} with SHA1: {exists.ContentSHA1}." );
                                        monitor.Info( $"{file.BinFolder.BinPath} has {file.ToString()} with SHA1: {file.ContentSHA1}." );
                                    }
                                }
                            }
                            else byName.Add( file.LocalFileName, file );
                        }
                    }
                    if( conflict ) return null;
                    dedupFiles = byName;
                }
                monitor.CloseGroup( $"{folders.Count} folders read." );
                return folders;
            }
        }

        static NormalizedPath GetWorkingDirectory(
            IActivityMonitor monitor,
            NormalizedPath workingDir,
            string ckSetupName,
            IReadOnlyList<BinFolder> folders,
            NormalizedPath basePath,
            int keepOnlyCount )
        {
            Debug.Assert( !basePath.IsEmptyPath );

            if( String.IsNullOrWhiteSpace( ckSetupName ) ) ckSetupName = null;

            string baseDir;

            NormalizedPath wDir;
            if( !workingDir.IsEmptyPath )
            {
                workingDir = basePath.Combine( workingDir ).ResolveDots();
                if( ckSetupName != null ) workingDir = workingDir.AppendPart( ckSetupName );

                wDir = Path.GetFullPath( workingDir );
                if( folders.Any( b => b.BinPath.StartsWith( wDir ) ) )
                {
                    monitor.Fatal( $"Working directory '{wDir}' cannot be inside one of the bin paths: '{folders.First(b => b.BinPath.StartsWith( wDir )).BinPath}'." );
                    return new NormalizedPath();
                }
                if( !Directory.Exists( wDir ) )
                {
                    monitor.Info( $"Creating provided Working Directory: {wDir}." );
                }
                baseDir = wDir + NormalizedPath.DirectorySeparatorChar;
                if( Directory.Exists( baseDir ) ) Cleanup( monitor, baseDir, keepOnlyCount );
                wDir = FileUtil.CreateUniqueTimedFolder( baseDir, null, DateTime.UtcNow );
                monitor.Info( $"Created setup folder in Working Directory: {wDir}." );
            }
            else
            {
                baseDir = Path.GetTempPath() + "CKSetup" + Path.DirectorySeparatorChar;
                if( ckSetupName != null ) baseDir += ckSetupName + Path.DirectorySeparatorChar;
                if( Directory.Exists( baseDir ) ) Cleanup( monitor, baseDir, keepOnlyCount );
                wDir = FileUtil.CreateUniqueTimedFolder( baseDir, null, DateTime.UtcNow );
                monitor.Info( $"Created temporary Working Directory: {wDir}." );
            }
            return wDir;
        }

        static void Cleanup( IActivityMonitor monitor, string baseDir, int keepOnlyCount )
        {
            Debug.Assert( Directory.Exists( baseDir ) );
            if( keepOnlyCount < 0 ) monitor.Warn( $"WorkingDirectory: KeepOnlyCount is negative, no Working Directory cleanup once." );
            else
            {
                using( monitor.OpenTrace( $"Cleaning WorkingDirectory with KeepOnlyCount = {keepOnlyCount}." ) )
                {
                    // EnumerateDirectories usually enumerates in odrer...
                    bool isSorted = true;
                    var all = new List<KeyValuePair<string,DateTime>>();
                    var prevTime = Util.UtcMinValue;
                    foreach( var dir in Directory.EnumerateDirectories( baseDir ) )
                    {
                        var fileName = Path.GetFileName( dir );
                        if( !FileUtil.TryParseFileNameUniqueTimeUtcFormat( fileName, out var time ) )
                        {
                            monitor.Warn( $"Found folder that is not a \"timed folder\": '{fileName}'. It is ignored." );
                            continue;
                        }
                        all.Add( new KeyValuePair<string, DateTime>( dir, time ) );
                        if( isSorted &= time >= prevTime ) prevTime = time;

                    }
                    monitor.Trace( $"Found {all.Count} timed folders." );
                    int extra = all.Count - keepOnlyCount;
                    if( extra >= 0 )
                    {
                        if( !isSorted ) all.Sort( ( dt1, dt2 ) => dt1.Value.CompareTo( dt2.Value ) );
                        foreach( var dt in all.Take( extra ) )
                        {
                            Facade.SafeDeleteFolder( monitor, dt.Key );
                        }
                    }
                }
            }
        }
    }
}
