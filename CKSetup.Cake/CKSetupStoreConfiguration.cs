using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cake.Core
{

    /// <summary>
    /// Centralized configuration for CKSetup methods.
    /// </summary>
    public class CKSetupStoreConfiguration
    {
        /// <summary>
        /// Gets or sets the temporary store path that will be used to register local components.
        /// Defaults to "CodeCakeBuilder/Releases/TempStore" and should not be changed.
        /// </summary>
        public string TemporaryStorePath { get; set; } = "CodeCakeBuilder/Releases/TempStore";

        /// <summary>
        /// Gets or sets a remote store url or full local path that will be used to
        /// resolve missing embedded components if added components rely
        /// on other components that are not produced locally.
        /// Defaults to null: the standard public store (<see cref="CKSetup.Facade.DefaultStoreUrl"/>) is used by default.
        /// To NOT use a remote store during registration, set this to "none".
        /// </summary>
        public string RemoteStoreUrl { get; set; }

        /// <summary>
        /// Gets or sets the target store url or full path to a local store where components will be pushed.
        /// Can be <see cref="CKSetup.Facade.DefaultStoreUrl"/> for public components (as long as the API key is known).
        /// </summary>
        public string TargetStoreUrl { get; set; }

        /// <summary>
        /// Gets or sets the API key that may be required by the <see cref="TargetStoreUrl"/>.
        /// </summary>
        public string TargetStoreAPIKey { get; set; }

        /// <summary>
        /// Gets whether this configuration is valid: it is simply that <see cref="TargetStoreUrl"/> is not null.
        /// </summary>
        public bool IsValid => TargetStoreUrl != null;

        /// <summary>
        /// Overridden to display the <see cref="TargetStoreUrl"/>, <see cref="RemoteStoreUrl"/> and whether
        /// this configuration has a non null or empty <see cref="TargetStoreAPIKey"/>.
        /// </summary>
        /// <returns>Readable string.</returns>
        public override string ToString()
        {
            return $"TargetStoreUrl: {TargetStoreUrl}, RemoteStoreUrl: {RemoteStoreUrl}, HasAPIKey: {!String.IsNullOrEmpty(TargetStoreAPIKey)}";
        }

        /// <summary>
        /// Creates a new <see cref="CKSetupStoreConfiguration"/> object (that can be mutated).
        /// Implements <see cref="CKSetupCakeContextExtensions.CKSetupCreateDefaultConfiguration(ICakeContext)"/>.
        /// </summary>
        /// <param name="targetStoreAPIKeyAndUrl">
        /// Can contain the API key and target url (or full local path) separated by a pipe (|)
        /// (or a target url alone without any pipe in it). Can be null.
        /// </param>
        /// <param name="remoteStoreUrl">
        /// If null, the target url from <paramref name="targetStoreAPIKeyAndUrl"/> is used (and if
        /// the target url is null the <see cref="CKSetupStoreConfiguration.RemoteStoreUrl"/> will default
        /// to <see cref="CKSetup.Facade.DefaultStoreUrl"/>).
        /// </param>
        /// <returns></returns>
        public static CKSetupStoreConfiguration CreateFromEnvironmentValues( string targetStoreAPIKeyAndUrl, string remoteStoreUrl )
        {
            string targetUrl = null;
            string targetAPIKey = null;
            if( String.IsNullOrWhiteSpace( targetStoreAPIKeyAndUrl ) ) targetStoreAPIKeyAndUrl = null;
            if( targetStoreAPIKeyAndUrl != null )
            {
                int idxKey = targetStoreAPIKeyAndUrl.IndexOf( '|' );
                if( idxKey >= 0 )
                {
                    targetAPIKey = targetStoreAPIKeyAndUrl.Substring( 0, idxKey );
                    targetUrl = targetStoreAPIKeyAndUrl.Substring( idxKey + 1 );
                }
                else targetUrl = targetStoreAPIKeyAndUrl;
            }
            if( remoteStoreUrl == null ) remoteStoreUrl = targetUrl;

            return new CKSetupStoreConfiguration()
            {
                RemoteStoreUrl = remoteStoreUrl,
                TargetStoreUrl = targetUrl,
                TargetStoreAPIKey = targetAPIKey
            };
        }


    }
}
