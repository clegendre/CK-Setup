using System;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Text;
using CK.Core;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace CKSetup.Runner
{
    public static partial class Program
    {
        public static bool SilentMode { get; private set; }
        public static string HeaderVersion { get; private set; }

        static public int Main( string[] args )
        {
            // This was because of https://github.com/dotnet/corefx/issues/23608
            // But we keep it: it is better to always use the english culture for a setup.
            CultureInfo.CurrentCulture
                = CultureInfo.CurrentUICulture
                = CultureInfo.DefaultThreadCurrentCulture
                = CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo( "en-US" );

            SilentMode = args.Any( a => a == "/silent" );
            HeaderVersion = GetHeaderVersion();
            if( !SilentMode ) Console.WriteLine( HeaderVersion );

            if( args.Any( a => a == "/launchDebug" ) )
            {
                Debugger.Launch();
            }

            string rawLogPath = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.RawLogs.txt" );
            if( File.Exists( rawLogPath ) ) File.Delete( rawLogPath );
            var rawLogText = new StringBuilder();
            rawLogText.AppendLine( HeaderVersion );

            IReadOnlyList<AssemblyLoadConflict> conflicts = null;
            try
            {
                using( var w = WeakAssemblyNameResolver.TemporaryInstall( c => conflicts = c ) )
                {
                    return ActualRunner.Run( rawLogText, args, () => w.Conflicts );
                }
            }
            catch( Exception ex )
            {
                rawLogText.Append( "Exception: " ).AppendLine( ex.Message );
                while( ex.InnerException != null )
                {
                    ex = ex.InnerException;
                    rawLogText.Append( " => Inner: " ).AppendLine( ex.Message );
                }
                return 1;
            }
            finally
            {
                if( conflicts != null && conflicts.Count > 0 )
                {
                    rawLogText.AppendLine( $"{conflicts.Count} assembly load conflicts:" );
                    foreach( var c in conflicts )
                    {
                        rawLogText.AppendLine( c.ToString() );
                    }
                }
                File.WriteAllText( rawLogPath, rawLogText.ToString() );
            }
        }

        static string GetHeaderVersion()
        {
            var a = (AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
            var thisFramework = Assembly.GetExecutingAssembly().CustomAttributes
               .Where( x => x.AttributeType.FullName == "System.Runtime.Versioning.TargetFrameworkAttribute" )
               .Select( x => x.ConstructorArguments )
               .Where( p => p.Count > 0 )
               .Select( p => p[0].Value as string )
               .FirstOrDefault();
            return "CKSetup.Runner " + (thisFramework ?? "<no TargetFrameworkAttribute>") + " - " + (a != null ? a.InformationalVersion : "<no version>");
        }
    }
}
