#if !NET461

#if NETCOREAPP3_0 || NETCOREAPP3_1
// Newtonsoft is not automatically available on .NET Core 3. System.Text.Json is.
#define USE_SYSTEM_TEXT
#else
#define USE_NEWTONSOFT
#endif

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CK.Core;
#if USE_NEWTONSOFT
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#endif
#if USE_SYSTEM_TEXT
using System.Text.Json;
using System.Text.Json.Serialization;
#endif

namespace CKSetup.Runner
{
    class RuntimeFrameworkSelector
    {
        private readonly List<FrameworkCandidate> _candidates;
        private readonly string[] _frameworksByPriority;

        public RuntimeFrameworkSelector()
        {
            _candidates = new List<FrameworkCandidate>();
            _frameworksByPriority = new[]
            {
                // Least global
                "Microsoft.NETCore.App",
                "Microsoft.AspNetCore.App",
                "Microsoft.AspNetCore.All",
                // Most global
            };
        }

        public void AddRuntimeConfigFile( IActivityMonitor m, string runtimeConfigPath )
        {
            string name = Path.GetFileName( runtimeConfigPath );

            var (frameworkName, frameworkVersion) = ReadRuntimeConfig( m, runtimeConfigPath );

            if( frameworkName != null && frameworkVersion != null )
            {
                m.Log( LogLevel.Info, $"{name}: {frameworkName} {frameworkVersion}" );
                _candidates.Add( new FrameworkCandidate( name, frameworkName, frameworkVersion ) );
            }
            else
            {
                m.Log( LogLevel.Info, $"{name}: Not using any shared framework." );
            }
        }

        static (string frameworkName, string frameworkVersion) ReadRuntimeConfig( IActivityMonitor m, string path )
        {
            string frameworkName = null;
            string frameworkVersion = null;
            try
            {
                using( StreamReader file = File.OpenText( path ) )
                {
#if USE_NEWTONSOFT
                    using( JsonTextReader reader = new JsonTextReader( file ) )
                    {
                        var token = JToken.ReadFrom( reader );
                        if( token is JObject root )
                        {
                            if( root.Property( "runtimeOptions" )?.Value is JObject runtimeOptions )
                            {
                                if( runtimeOptions.Property( "name" )?.Value is JValue name )
                                {
                                    frameworkName = name.Value as string;
                                    // Not present in SCD .runtimeconfig.json files
                                }

                                if( runtimeOptions.Property( "version" )?.Value is JValue version )
                                {
                                    frameworkVersion = version.Value as string;
                                    // Not present in SCD .runtimeconfig.json files
                                }
                            }
                            else
                            {
                                m.Log( LogLevel.Error, $"Failed to read RuntimeConfig from {path}: Missing object runtimeOptions." );
                            }
                        }
                        else
                        {
                            m.Log( LogLevel.Error, $"Failed to read RuntimeConfig from {path}: Root is not an object." );
                        }
                    }
#endif
#if USE_SYSTEM_TEXT
                    using( var doc = JsonDocument.Parse( file.ReadToEnd() ) )
                    {
                        var jFrameworkInfo = doc.RootElement.GetProperty( "runtimeOptions" ).GetProperty( "framework" );
                        frameworkName = jFrameworkInfo.GetProperty( "name" ).GetString();
                        frameworkVersion = jFrameworkInfo.GetProperty( "version" ).GetString();
                    }
#endif
                }

            }
            catch( Exception e )
            {
                m.Log( LogLevel.Error, $"Failed to read RuntimeConfig from {path}.", e );
            }
            return (frameworkName, frameworkVersion);
        }

        public FrameworkCandidate GetBestSharedFramework( IActivityMonitor m )
        {
            // TODO: Not this.
            int sharedFrameworkNameIdx = 0;
            string sharedFrameworkVersion = null;

            foreach( var candidate in _candidates )
            {
                int idx = Array.IndexOf( _frameworksByPriority, candidate.FrameworkName );
                if( idx < 0 )
                {
                    throw new InvalidOperationException(
                        $"Runtime framework \"{candidate.FrameworkName}\" is not supported ({candidate.Name})." );
                }
                else if( idx > sharedFrameworkNameIdx )
                {
                    sharedFrameworkNameIdx = idx;
                }


                if( sharedFrameworkVersion == null )
                {
                    sharedFrameworkVersion = candidate.FrameworkVersion;
                }
                else if( sharedFrameworkVersion != candidate.FrameworkVersion )
                {
                    throw new InvalidOperationException(
                        $"Runtime framework version \"{candidate.FrameworkVersion}\" from {candidate.Name} must match other framework versions ({sharedFrameworkVersion}." );
                }
            }

            if( sharedFrameworkVersion == null )
                throw new InvalidOperationException( $"No framework could be read from .runtime.config files." );

            m.Log( LogLevel.Info,
                $"CKSetup.Runner will use the shared framework: {_frameworksByPriority[sharedFrameworkNameIdx]} {sharedFrameworkVersion}." );

            return new FrameworkCandidate( null, _frameworksByPriority[sharedFrameworkNameIdx],
                sharedFrameworkVersion );
        }

        internal class FrameworkCandidate
        {
            public FrameworkCandidate( string name, string frameworkName, string frameworkVersion )
            {
                Name = name;
                FrameworkName = frameworkName;
                FrameworkVersion = frameworkVersion;
            }

            public string Name { get; }
            public string FrameworkName { get; }
            public string FrameworkVersion { get; }
        }

        public void ReplaceFramework( IActivityMonitor m, string runnerConfigPath, FrameworkCandidate bestFramework,
            string runnerConfigTargetPath )
        {
#if USE_NEWTONSOFT
            // Load JObject
            JObject o;
            using( StreamReader file = File.OpenText( runnerConfigPath ) )
            using( JsonTextReader reader = new JsonTextReader( file ) )
            {
                o = (JObject)JToken.ReadFrom( reader );
            }

            // Edit in-place
            o["runtimeOptions"]["framework"]["name"] = bestFramework.FrameworkName;
            o["runtimeOptions"]["framework"]["version"] = bestFramework.FrameworkVersion;

            // Save
            using( var fs = File.Open( runnerConfigTargetPath, FileMode.Create, FileAccess.Write, FileShare.Read ) )
            using( var sw = new StreamWriter( fs, Encoding.UTF8, 4096, true ) )
            using( JsonTextWriter writer = new JsonTextWriter( sw ) )
            {
                // Defaults from runtimeconfig.json files
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.IndentChar = ' ';

                o.WriteTo( writer );
            }
#endif
#if USE_SYSTEM_TEXT
            // https://github.com/dotnet/corefx/issues/39922
            // https://github.com/dotnet/corefx/issues/42056
            // TODO: .NET Core 3
            m.Log( LogLevel.Error, $"System.Text.Json (.NET Core 3) does not support edit-and-reserialize scenarios at this time. No change was made to file: \"{runnerConfigTargetPath}\"." );
#endif
        }
    }
}
#endif
