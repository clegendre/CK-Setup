using CK.Core;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using CK.Monitoring.InterProcess;
using System.Collections.Generic;
using System.Linq;

#if !NET461
using Microsoft.Extensions.DependencyModel;
#endif

namespace CKSetup.Runner
{
    static class ActualRunner
    {
        static public int Run( StringBuilder rawLogText, string[] args, Func<IReadOnlyList<AssemblyLoadConflict>> monitoredConflicts )
        {
            SimplePipeSenderActivityMonitorClient pipeClient;
            IActivityMonitor monitor = CreateMonitor( rawLogText, args, out pipeClient );
            using( monitor.OpenLog( LogLevel.Info, $"Starting {Program.HeaderVersion}" ) )
            {
                try
                {
                    if( Array.IndexOf( args, "merge-deps" ) >= 0 )
                    {
                        MergeDeps( monitor );
                        return 0;
                    }
                    XElement root = XDocument.Load( Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.Config.xml" ) ).Root;
                    string engineAssemblyQualifiedName = root.Attribute( "Engine" )?.Value;
                    if( string.IsNullOrWhiteSpace( engineAssemblyQualifiedName ) )
                    {
                        monitor.Log( LogLevel.Fatal, "Engine attribute element missing or empty." );
                        return 3;
                    }

                    if( !LogFilter.TryParse( (string)root.Element( "LogLevel" ) ?? String.Empty, out var level ) ) level = LogFilter.Trace;
                    monitor.MinimalFilter = level;

                    Type runnerType = SimpleTypeFinder.WeakResolver( engineAssemblyQualifiedName, true );
                    object runner = Activator.CreateInstance( runnerType, monitor, root );
                    MethodInfo m = runnerType.GetMethod( "Run" );
                    if( m.ReturnType == typeof( bool ) )
                    {
                        return (bool)m.Invoke( runner, Array.Empty<object>() ) ? 0 : 3;
                    }
                    monitor.Log( LogLevel.Warn, $"Runner '{runnerType}': Run method should return a boolean." );
                    m.Invoke( runner, Array.Empty<object>() );
                    return 0;
                }
                catch( Exception ex )
                {
                    monitor.Log( LogLevel.Fatal, ex.Message, ex );
                    return 2;
                }
                finally
                {
                    var conflicts = monitoredConflicts();
                    if( conflicts.Count == 0 )
                    {
                        monitor.Log( LogLevel.Info, "No assembly load conflicts." );
                    }
                    else using( monitor.OpenLog( LogLevel.Warn, $"{conflicts.Count} assembly load conflicts:" ) )
                    {
                        foreach( var c in conflicts )
                        {
                            monitor.Log( LogLevel.Warn, c.ToString() );
                        }
                    }
                    pipeClient?.Dispose();
                }
            }
        }

        static IActivityMonitor CreateMonitor( StringBuilder rawLogText, string[] args, out SimplePipeSenderActivityMonitorClient pipeClient )
        {
            pipeClient = null;
            var monitor = new ActivityMonitor();
            bool hasLogPipe = false;
            foreach( var a in args )
            {
                if( a.StartsWith( "/logPipe:" ) && a.Length > 9 )
                {
                    pipeClient = new SimplePipeSenderActivityMonitorClient( a.Substring( 9 ) );
                    monitor.Output.RegisterClient( pipeClient );
                    hasLogPipe = true;
                }
            }
            if( !hasLogPipe )
            {
                if( Program.SilentMode )
                {
                    rawLogText.AppendLine( "Missing /logPipe: parameter. Running in /silent mode." );
                }
                else
                {
                    monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
                    monitor.Log( LogLevel.Warn, "Missing /logPipe: parameter. Using Console." );
                }
            }
            return monitor;
        }

        static void MergeDeps( IActivityMonitor m )
        {
#if NET461
            throw new ArgumentException( "Invalid merge-deps argument in .Net Framework." );
#else

            using( m.OpenLog( LogLevel.Info, "Merging DependencyContext from .deps.json files." ) )
            using( var dcrw = new DependencyContextRewriter() )
            {
                var depsJsonFiles = Directory.EnumerateFiles( AppContext.BaseDirectory, "*.deps.json" );
                foreach( var depsJsonFile in depsJsonFiles )
                {
                    var name = Path.GetFileName( depsJsonFile );
                    if( name == "CKSetup.Runner.deps.json" ) continue; // Skip dependencies from CKSetup.Runner

                    dcrw.AddDependencyContext( m, depsJsonFile, name );
                }

                var c = dcrw.Build( m );

                // The .deps.json.merged will be swapped with the original .deps.json
                // in CKSetup.Facade.RunSetupRunner().
                string mergedFileName = "CKSetup.Runner.deps.json.merged";
                m.Log( LogLevel.Info, $"Saving merged DependencyContext to \"{mergedFileName}\"." );
                var path = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.deps.json.merged" );
                var writer = new DependencyContextWriter();
                using( var output = File.Open( path, FileMode.Create, FileAccess.Write, FileShare.None ) )
                {
                    writer.Write( c, output );
                }
            }

            using( m.OpenLog( LogLevel.Info, "Selecting shared framework from .runtimeconfig.json files." ) )
            {
                // Include CKSetup.Runner.runtimeconfig.json this time
                string runnerConfigPath = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.runtimeconfig.json" );
                // The .runtimeconfig.json.merged will be swapped with the original .runtimeconfig.json
                // in CKSetup.Facade.RunSetupRunner().
                string runnerConfigTargetPath = Path.Combine( AppContext.BaseDirectory, "CKSetup.Runner.runtimeconfig.json.merged" );
                if( !File.Exists( runnerConfigPath ) )
                {
                    throw new FileNotFoundException( "CKSetup.Runner.runtimeconfig.json file not found", runnerConfigPath );
                }
                var runtimeConfigFiles = Directory.EnumerateFiles( AppContext.BaseDirectory, "*.runtimeconfig.json" );
                var s = new RuntimeFrameworkSelector();

                foreach( var configFile in runtimeConfigFiles )
                {
                    s.AddRuntimeConfigFile( m, configFile );
                }
                var bestFramework = s.GetBestSharedFramework( m );

                s.ReplaceFramework( m, runnerConfigPath, bestFramework, runnerConfigTargetPath );
            }
#endif
        }
    }
}
