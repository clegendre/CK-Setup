using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    class StoreBinFolderArguments
    {
        public StoreBinFolderArguments( CommandArgument arg )
        {
            CommandArgument = arg;
        }

        public CommandArgument CommandArgument { get; }

        public IReadOnlyList<BinFolder> Folders { get; private set; }
        
        public bool Initialize( ConsoleMonitor m )
        {
            Folders = Facade.ReadBinFolders( m, CommandArgument.Values );
            return Folders != null;
        }

    }
}
