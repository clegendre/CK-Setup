using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// A <see cref="IActivityMonitor"/> implementation that is configured by the command line options.
    /// </summary>
    class ConsoleMonitor : IActivityMonitor, IDisposable
    {
        readonly IActivityMonitor _m;
        readonly CommandLineApplication _c;
        BufferedFileMonitorClient _fileWriter;

        public ConsoleMonitor( CommandLineApplication c, string consoleFilter, string logFilePath )
        {
            _c = c;
            _m = new ActivityMonitor();
            var consoleClient = new ColoredActivityMonitorConsoleClient();
            _m.Output.RegisterClient( consoleClient );

            // Handle "--verbosity / -v" command option: this is the minimal filter of the console client
            // so that a more verbose LogLevel can be used in the Stup configuration for the Logs that
            // are by default in the WorkingDirectory.
            if( consoleFilter != null )
            {
                if( !LogFilter.TryParse( consoleFilter, out var filter ) )
                {
                    _m.Warn( $"Unrecognized LogFiler value ('{consoleFilter}'). Using default Verbose level for Console Monitoring. {CommandLineApplicationExtension.LogFilterDesc}" );
                }
                else consoleClient.MinimalFilter = filter;
            }

            // Handle "--logFile / -l" command option.
            // This log file catches everything that is logged (it doesn't have a MinimalFilter): if the Setup configuration
            // specifies a more verbose level than the -v command line option, these detailed logs will be available in the log file.
            if( !string.IsNullOrWhiteSpace( logFilePath ) )
            {
                _fileWriter = BufferedFileMonitorClient.RegisterClient( _m, logFilePath );
            }
        }

        public CKTrait AutoTags { get => _m.AutoTags; set => _m.AutoTags = value; }

        public LogFilter MinimalFilter { get => _m.MinimalFilter; set => _m.MinimalFilter = value; }

        public LogFilter ActualFilter => _m.ActualFilter;

        public string Topic => _m.Topic;

        public IActivityMonitorOutput Output => _m.Output;

        public DateTimeStamp LastLogTime => _m.LastLogTime;

        public bool CloseGroup( DateTimeStamp logTime, object userConclusion = null ) => _m.CloseGroup( logTime, userConclusion );

        public void Dispose()
        {
            _m.MonitorEnd();
            if( _fileWriter != null )
            {
                _fileWriter.Dispose();
                _fileWriter = null;
            }
        }

        public void SetTopic( string newTopic, [CallerFilePath] string fileName = null, [CallerLineNumber] int lineNumber = 0 )
            => _m.SetTopic( newTopic, fileName, lineNumber );

        public void UnfilteredLog( ActivityMonitorLogData data ) => _m.UnfilteredLog( data );

        public IDisposableGroup UnfilteredOpenGroup( ActivityMonitorGroupData data ) => _m.UnfilteredOpenGroup( data );

        public int SendErrorAndDisplayHelp( string msg )
        {
            _m.Error( msg );
            _c.ShowHelp();
            return Program.RetCodeError;
        }

        public int SendError( string msg )
        {
            _m.Error( msg );
            return Program.RetCodeError;
        }
    }

}
