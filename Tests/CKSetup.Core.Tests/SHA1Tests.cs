using CK.Core;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class SHA1Tests
    {
        static string GetFilePath( [CallerFilePath]string p = null ) => p;

        [Test]
        public async Task SHA1_from_file_async()
        {
            var sha = SHA1Value.ComputeFileSHA1( GetFilePath() );
            var sha2 = await SHA1Value.ComputeFileSHA1Async( GetFilePath() );
            sha2.Should().Be( sha );
            using( var compressedPath = new TemporaryFile() )
            {
                using( var input = new FileStream( GetFilePath(), FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                using( var compressed = new FileStream( compressedPath.Path, FileMode.Truncate, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                {
                    var writer = StreamStore.StreamStoreExtension.GetCompressShellAsync( w => input.CopyToAsync( w ) );
                    await writer( compressed );
                }
                var shaCompressed = await SHA1Value.ComputeFileSHA1Async( compressedPath.Path );
                shaCompressed.Should().NotBe( sha );
                var localSha = await SHA1Value.ComputeFileSHA1Async( compressedPath.Path, r => new GZipStream( r, CompressionMode.Decompress, true ) );
                localSha.Should().Be( sha );
            }
        }


    }
}
