using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using CKSetup.StreamStore;
using System.IO;
using FluentAssertions;
using static CK.Testing.CKSetupTestHelper;
using CK.Testing;
using CK.Testing.CKSetup;
using System.Xml.Linq;
using CK.Text;
using CSemVer;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class LocalStoreTests
    {
        static string RegisterInStoreWithAllComponents()
        {
            string sourcePath = TestHelper.CKSetup.GetTestStorePath( null, "StoreWithAllComponents" );
            TestHelper.OnlyOnce( () =>
            {
                TestHelper.CKSetup.RemoveComponentsFromStore( c => true, true, sourcePath );
                LocalHelper.RegisterAllComponents( sourcePath );

            }, $"push_to_StoreWithAllComponents_local_remote" );
            return sourcePath;
        }

        [Obsolete( "v10 or v11 version will support only Directory based store: use the GetTestStorePath() without store type." )]
        static string RegisterInStoreWithAllComponents( CKSetupStoreType sourceType )
        {
            string sourcePath = TestHelper.CKSetup.GetTestStorePath( sourceType, null, "StoreWithAllComponents" );
            TestHelper.OnlyOnce( () =>
            {
                TestHelper.CKSetup.RemoveComponentsFromStore( c => true, true, sourcePath );
                LocalHelper.RegisterAllComponents( sourcePath );

            }, $"push_to_StoreWithAllComponents_local_remote:{sourceType}" );
            return sourcePath;
        }

        public void push_to_a_local_remote()
        {
            string sourcePath = RegisterInStoreWithAllComponents();

            string targetPath = TestHelper.CKSetup.GetCleanTestStorePath();
            // Ensures that the target store is created.
            LocalStore.OpenOrCreate( TestHelper.Monitor, targetPath ).Dispose();

            using( var source = LocalStore.Open( TestHelper.Monitor, sourcePath ) )
            {
                var targetUri = new Uri( targetPath );
                source.PushComponents( c => true, targetUri, null ).Should().BeTrue();
            }
            var sourceContent = new StoreContent( sourcePath );
            var targetContent = new StoreContent( targetPath );
            sourceContent.ShouldBeEquivalentTo( targetContent );
        }

        [Test]
        public void running_with_missing_dependencies_updates_the_local_store()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath();

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore, remotePath, forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            // Our local store has the shared Facade.DefaultStorePath as its prototype.
            // When runnig on a dirty, non commited repository, the CKSetup.Runner/0.0.0-0 may have been
            // resolved by the shared store to an existing release version (greater than the zero version).
            // And the StupidEngine requires the CKSetup.Runner with a minDepencyVersion of 8.0.1--0026-develop.
            // this why 2 <= localContent.ComponentDB.Components.Count <= 4.
            var localContent = new StoreContent( localStore );
            localContent.ComponentDB.Components.Count.Should().BeInRange( 2, 4 );

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );
        }

        public void running_with_prototype_store_updates_the_local_store()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath();
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore );

            TestHelper.CKSetup.Run( conf, localStore, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );
        }

        static bool StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( string path )
        {
            var localContent = new StoreContent( path );
            var components = localContent.ComponentDB.Components;
            if( components.Count == 0 ) return false;
            if( components.Count == 3 )
            {
                // We have 2 CKSetup.Runner (the current build version and the
                // 8.0.1--0026-develop that is the minDependencyVersion of CKSetup.Runner
                // by the required setup dependency of the StupidEngine) plus
                // the StupidEngine itself.
                var v = new InformationalVersion( typeof( Facade ).Assembly
                                                    .GetCustomAttributes( false )
                                                    .OfType<System.Reflection.AssemblyInformationalVersionAttribute>()
                                                    .Single()
                                                    .InformationalVersion );
                components.Any( c => c.Name == "CKSetup.Runner" && c.Version == v.Version )
                    .Should().BeTrue( $"The CKSetup.Runner/{v.Version} must exist but available versions are: {components.Where( c => c.Name == "CKSetup.Runner" ).Select( c => c.Version.ToString() ).Concatenate()}. Did you miss a rebuild of CKSetup.Runner?" );
            }
            else if( components.Count != 2 )
            {
                Assert.Fail( "There should be only 2 or 3 components." );
            }
            return true;
        }


        [Test]
        public void chaining_stores_with_prototype()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore1 = TestHelper.CKSetup.GetCleanTestStorePath( "Level1" );
            string localStore2 = TestHelper.CKSetup.GetCleanTestStorePath( "Level2" );
            string localStore3 = TestHelper.CKSetup.GetCleanTestStorePath( "Level3" );
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore1 ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore2 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore1, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore3 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore2, UriKind.Absolute );
            }
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore1 ).Should().BeFalse();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore2 ).Should().BeFalse();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore3 ).Should().BeFalse();

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore3, "none", forceSetup: true )
                .Should().Be( CKSetupRunResult.Succeed );

            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore1 ).Should().BeTrue();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore2 ).Should().BeTrue();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore3 ).Should().BeTrue();
        }

        [Test]
        public void importing_absolutely_no_components_does_not_throw()
        {
            NormalizedPath remote = TestHelper.CKSetup.GetTestStorePath( "Remote" );
            NormalizedPath local = TestHelper.CKSetup.GetTestStorePath( "Local" );

            using( var rem = LocalStore.OpenOrCreate( TestHelper.Monitor, remote ) )
            using( var loc = LocalStore.OpenOrCreate( TestHelper.Monitor, local ) )
            {
                var unexisting = new[] { new ComponentRef( "Does.Not.Exist", TargetFramework.Net461, SVersion.ZeroVersion ) };
                ComponentMissingDescription missing = new ComponentMissingDescription( unexisting );
                using( var mem = new MemoryStream() )
                {
                    rem.ExportComponents( missing, mem, TestHelper.Monitor );
                    mem.Position = 0;
                    loc.ImportComponents( mem, rem.Remote ).Should().BeTrue();
                }
            }
        }

        class FakeDownloader : IComponentFileDownloader
        {
            public Uri Url => new Uri( "https://fake.fake" );

            public StoredStream GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind kind )
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void importing_from_an_empty_or_invalid_stream_does_not_throw_but_is_an_error()
        {
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath();

            using( var store = LocalStore.OpenOrCreate( TestHelper.Monitor, storePath ) )
            {
                using( var mem = new MemoryStream() )
                {
                    store.ImportComponents( mem, new FakeDownloader() ).Should().BeFalse();
                }
            }
        }


        [Test]
        public void mapping_CKSetupAutoTargetProjectBinFolder_in_BinPaths()
        {
            SetupConfiguration conf = new SetupConfiguration();
            conf.BinPaths.Add( "{CKSetupAutoTargetProjectBinFolder}" );
            conf.BinPaths.Add( $"{TestHelper.BinFolder}/{{CKSetupAutoTargetProjectBinFolder}}" );
            conf.BinPaths.Add( $"ThisIsIgnored/{{CKSetupAutoTargetProjectBinFolder}}/../../.." );
            conf.BinPaths.Add( $"{{CKSetupAutoTargetProjectBinFolder}}/../netcoreapp2.1/publish" );
            EventHandler<SetupRunningEventArgs> hook = ( o, e ) =>
             {
                 e.Configuration.BinPaths.Should().HaveCount( 4 );
                 e.Configuration.BinPaths.ElementAt( 0 ).Path.Should().StartWith( $"{TestHelper.RepositoryFolder}/CKSetup.Core/bin/{TestHelper.BuildConfiguration}" );
                 e.Configuration.BinPaths.ElementAt( 1 ).Path.Should().StartWith( $"{TestHelper.RepositoryFolder}/CKSetup.Core/bin/{TestHelper.BuildConfiguration}" );
                 e.Configuration.BinPaths.ElementAt( 2 ).Path.Should().Be( $"{TestHelper.RepositoryFolder}/CKSetup.Core" );
                 var toNetCore20Published = e.Configuration.BinPaths.ElementAt( 3 ).Path;
                 toNetCore20Published.Should().Be( $"{TestHelper.RepositoryFolder}/CKSetup.Core/bin/{TestHelper.BuildConfiguration}/netcoreapp2.1/publish" );
                 e.Cancel = true;
             };
            TestHelper.CKSetup.SetupRunning += hook;
            try
            {
                TestHelper.CKSetup.Run( conf ).Should().Be( CKSetupRunResult.None );
            }
            finally
            {
                TestHelper.CKSetup.SetupRunning -= hook;
            }
        }


    }
}
