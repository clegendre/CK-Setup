using CK.Core;
using CK.Text;
using CSemVer;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class SetupConfigurationTests
    {
        [Test]
        public void SharedConfigurationMode_is_false_by_default()
        {
            var c = new SetupConfiguration();
            CheckEmpty( c, false );
            ConfigureSetupConfiguration( c );
            CheckIndependentModeXml( c );
        }

        [Test]
        public void SharedConfigurationMode_works_the_same()
        {
            var c = new SetupConfiguration( sharedMode: true );
            CheckEmpty( c, true );
            ConfigureSetupConfiguration( c );
            CheckSharedModeXml( c );
        }

        static void CheckSharedModeXml( SetupConfiguration c )
        {
            XElement expected = XElement.Parse( @"
<CKSetup Engine=""Engine"">
    <Dependencies>
        <Dependency Name=""Dep0"" MinVersion=""0.0.0-0"" />
        <Dependency Source=""Test"" Name=""Dep1"" MinVersion=""1.0.0"" />
    </Dependencies>
    <BinPaths>
        <BinPath Path=""P1"" />
        <BinPath Path=""P2"" />
    </BinPaths>
    <BasePath>/Base/Path</BasePath>
    <ExternalSignature>ext-signature</ExternalSignature>
    <PreferredTargetRuntimes>Net462;NetCoreApp11</PreferredTargetRuntimes>
    <UseSignatureFiles>false</UseSignatureFiles>
    <WorkingDirectory>/Temp/WorkDir</WorkingDirectory>
</CKSetup>
" );
            Assert.That( c.SharedConfigurationMode );
            Assert.That( c.UseSignatureFiles == false, "The default for this UseSignatureFiles is to be true." );
            var e = c.ToXml();
            e.Root.Should().BeEquivalentTo( expected );

            var eForSign = c.ToXml( forSignature: true );
            expected.Element( "UseSignatureFiles" ).Remove();
            eForSign.Root.Should().BeEquivalentTo( expected );
        }

        static void CheckIndependentModeXml( SetupConfiguration c )
        {
            XElement expected = XElement.Parse( @"
<Root>
  <CKSetup>
    <Dependencies>
      <Dependency Name=""Dep0"" MinVersion=""0.0.0-0"" />
      <Dependency Source=""Test"" Name=""Dep1"" MinVersion=""1.0.0"" />
    </Dependencies>
    <BinPaths>
      <BinPath Path=""P1"" />
      <BinPath Path=""P2"" />
    </BinPaths>
    <BasePath>/Base/Path</BasePath>
    <EngineAssemblyQualifiedName>Engine</EngineAssemblyQualifiedName>
    <ExternalSignature>ext-signature</ExternalSignature>
    <PreferredTargetRuntimes>Net462;NetCoreApp11</PreferredTargetRuntimes>
    <UseSignatureFiles>false</UseSignatureFiles>
    <WorkingDirectory>/Temp/WorkDir</WorkingDirectory>
  </CKSetup>
  <Configuration />
</Root>
" );
            Assert.That( !c.SharedConfigurationMode );
            Assert.That( c.UseSignatureFiles == false, "The default for this UseSignatureFiles is to be true." );
            var e = c.ToXml();
            e.Root.Should().BeEquivalentTo( expected );

            var eForSign = c.ToXml( forSignature: true );
            expected.Element( "CKSetup" ).Element( "UseSignatureFiles" ).Remove();
            eForSign.Root.Should().BeEquivalentTo( expected );
        }

        static void ConfigureSetupConfiguration( SetupConfiguration c )
        {
            c.BasePath = "/Base/Path";
            c.BinPaths.Clear();
            c.BinPaths.AddRangeArray( "P1", "P2" );
            c.Dependencies.Clear();
            c.Dependencies.AddRangeArray( new SetupDependency( "Dep0", useMinVersion: SVersion.ZeroVersion ),
                                          new SetupDependency( "Dep1", useMinVersion: SVersion.Parse( "1.0.0" ), sourceName: "Test" ) );
            c.EngineAssemblyQualifiedName = "Engine";
            c.ExternalSignature = "ext-signature";
            c.PreferredTargetRuntimes.Clear();
            c.PreferredTargetRuntimes.AddRangeArray( TargetRuntime.Net462, TargetRuntime.NetCoreApp11 );
            c.UseSignatureFiles = false;
            c.WorkingDirectory = "/Temp/WorkDir";
        }

        static void CheckEmpty( SetupConfiguration c, bool expectedSharedMode )
        {
            c.SharedConfigurationMode.Should().Be( expectedSharedMode );
            c.BasePath.Should().Be( new NormalizedPath() );
            c.BinPaths.Should().BeEmpty();
            c.Dependencies.Should().BeEmpty();
            c.EngineAssemblyQualifiedName.Should().BeEmpty();
            c.ExternalSignature.Should().BeEmpty();
            c.PreferredTargetRuntimes.Should().BeEmpty();
            c.UseSignatureFiles.Should().BeTrue();
            c.WorkingDirectory.Should().Be( new NormalizedPath() );

            if( !expectedSharedMode )
            {
                c.Configuration.Name.Should().Be( SetupConfiguration.xConfigurationDefaultName );
                c.Configuration.Elements().Should().BeEmpty();
                c.Configuration.Attributes().Should().BeEmpty();
            }
            else
            {
                c.Configuration.Should().BeEquivalentTo( new XElement( "CKSetup", new XElement( "Dependencies" ), new XElement( "BinPaths" ) ) );
            }
        }
    }
}
