using CK.Core;
using CK.Testing;
using CK.Text;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class RunningStupidEngineTests
    {
        [Test]
        public void checkConfig()
        {
            TestHelper.CKSetup.DefaultStorePath.Should().Be( TestHelper.TestProjectFolder.Combine( "../TempStore" ).ResolveDots() );
        }

        [TearDown]
        public void ResetCKSetupWorkingDirectory()
        {
            TestHelper.CKSetup.WorkingDirectory = null;
        }

        //[TestCase( "6.0.0-a00-00-develop-0032", null, false )]
        [TestCase( "UseLocalRunner", "SetupWorkingDir", true )]
        [TestCase( "UseLocalRunner", "SetupWorkingDir", false )]
        public void run_setup_on_StupidModel_net461( string runnerVersion, string setupWorkingDir, bool useShareConfiguration )
        {
            if( setupWorkingDir != null )
            {
                TestHelper.CKSetup.WorkingDirectory = TestHelper.TestProjectFolder.Combine( setupWorkingDir );
            }
            bool localRunner = runnerVersion == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath();

            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { LocalHelper.StupidEngineNet461, LocalHelper.StupidModelNet461 }, storePath )
                .Should().BeTrue();

            var conf = new SetupConfiguration( useShareConfiguration );
            if( localRunner )
            {
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { LocalHelper.RunnerNet461 }, storePath )
                    .Should().BeTrue();
                var runningVersion = CSemVer.InformationalVersion.ReadFromFile( LocalHelper.RunnerNet461.AppendPart( "CKSetup.Runner.exe" ) ).Version;
                conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", runningVersion ) );
            }
            else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );

            // Using BasePath here:
            conf.BasePath = TestHelper.SolutionFolder;
            conf.BinPaths.Add( LocalHelper.StupidModelNet461.RemovePrefix( TestHelper.SolutionFolder ) );

            CommonRun( useShareConfiguration, localRunner, storePath, conf );
        }

        //[TestCase( "8.0.0", false )]
        [TestCase( "UseLocalRunner", true )]
        [TestCase( "UseLocalRunner", false )]
        public void run_setup_on_StupidModel_netcore( string runnerVersion, bool useShareConfiguration )
        {
            bool localRunner = runnerVersion == "UseLocalRunner";
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath();

            var r = TestHelper.CKSetup.EnsurePublished( LocalHelper.StupidEngineNetCoreApp21 );
            r.Success.Should().BeTrue();
            TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { r.PublishedPath, LocalHelper.StupidModelNetStandard20 }, storePath )
                .Should().BeTrue();

            var conf = new SetupConfiguration( useShareConfiguration );
            conf.PreferredTargetRuntimes.Add( TargetRuntime.NetCoreApp21 );
            conf.CKSetupName = $"StupidModelNetCore({runnerVersion},{(useShareConfiguration ? "SharedConfigurationMode" : "IndependentConfigurationMode")})";
            if( localRunner )
            {
                r = TestHelper.CKSetup.EnsurePublished( LocalHelper.RunnerNetCoreApp21 );
                r.Success.Should().BeTrue();
                TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { r.PublishedPath }, storePath )
                    .Should().BeTrue();

                var runningVersion = CSemVer.InformationalVersion.ReadFromFile( r.PublishedPath.AppendPart( "CKSetup.Runner.dll" ) ).Version;
                conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", runningVersion ) );
            }
            else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );

            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );

            CommonRun( useShareConfiguration, localRunner, storePath, conf );
        }

        private static void CommonRun( bool useShareConfiguration, bool localRunner, NormalizedPath storePath, SetupConfiguration conf )
        {
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            if( !useShareConfiguration ) conf.Configuration = new XElement( "StupidConf" );

            conf.LogLevel = LogFilter.Verbose;
            IReadOnlyList<ActivityMonitorSimpleCollector.Entry> entries = null;
            using( TestHelper.Monitor.CollectEntries( e => entries = e, LogLevelFilter.Debug ) )
            {
                TestHelper.CKSetup.Run( conf, storePath, remoteStoreUrl: localRunner ? "none" : null, launchDebug: false, forceSetup: true )
                    .Should().Be( CKSetupRunResult.Succeed );
            }
            entries.Should().Contain( e => e.Text == "Initialized new stupid Engine." );
            if( localRunner )
            {
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Verbose)." );
            }
            else
            {
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Undefined)." );
            }
            TestHelper.CKSetup.Run( conf, storePath, remoteStoreUrl: localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                .Should().Be( CKSetupRunResult.UpToDate, "Signatures match: Setup is skipped" );

            conf.ExternalSignature = "something";
            using( TestHelper.Monitor.CollectEntries( e => entries = e, LogLevelFilter.Debug ) )
            {
                TestHelper.CKSetup.Run( conf, storePath, remoteStoreUrl: localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                    .Should().Be( CKSetupRunResult.Succeed );
            }
            entries.Should().Contain( e => e.Text == "Initialized new stupid Engine." );
            if( localRunner )
            {
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Verbose)." );
            }
            else
            {
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Undefined)." );
            }

            conf.LogLevel = LogFilter.Debug;
            using( TestHelper.Monitor.CollectEntries( e => entries = e, LogLevelFilter.Debug ) )
            {
                TestHelper.CKSetup.Run( conf, storePath, remoteStoreUrl: localRunner ? "none" : null, launchDebug: false, forceSetup: false )
                    .Should().Be( CKSetupRunResult.Succeed );
            }
            entries.Should().Contain( e => e.Text == "Initialized new stupid Engine." );
            if( localRunner )
            {
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Debug)." );
            }
            else
            {
                // There were no LogLevel in 8.0.0.
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Undefined)." );
            }
        }

        //[TestCase( "UseLocalRunner", TargetRuntime.NetCoreApp21 )]
        //[TestCase( "UseLocalRunner", TargetRuntime.NetCoreApp22 )]
        //public void run_setup_on_StupidModel_netcore21_or_22( string runnerVersion, TargetRuntime preferred )
        //{
        //    bool localRunner = runnerVersion == "UseLocalRunner";
        //    NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath( CKSetupStoreType.Directory );

        //    string pubEnginePath21 = TestHelper.CKSetup.DotNetPublish( LocalHelper.StupidEngineNetCoreApp21 );
        //    string pubEnginePath22 = TestHelper.CKSetup.DotNetPublish( LocalHelper.StupidEngineNetCoreApp22 );
        //    TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubEnginePath21, pubEnginePath21, LocalHelper.StupidModelNetStandard20 }, storePath )
        //        .Should().BeTrue();

        //    var conf = new SetupConfiguration();
        //    if( localRunner )
        //    {
        //        string pubRunnerPath21 = TestHelper.CKSetup.DotNetPublish( LocalHelper.RunnerNetCoreApp21 );
        //        string pubRunnerPath22 = TestHelper.CKSetup.DotNetPublish( LocalHelper.RunnerNetCoreApp22 );
        //        TestHelper.CKSetup.AddComponentFoldersToStore( new string[] { pubRunnerPath21, pubRunnerPath21 }, storePath )
        //            .Should().BeTrue();
        //    }
        //    else conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", CSemVer.SVersion.Parse( runnerVersion ) ) );

        //    NormalizedPath modelPath = LocalHelper.StupidModelNetStandard20;

        //    conf.BinPaths.Add( modelPath );
        //    conf.Configuration = new XElement( "StupidConf" );
        //    conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";
        //    conf.PreferredTargetRuntimes.Add( preferred );

        //    TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: true )
        //        .Should().Be( CKSetupRunResult.Succeed );

        //    TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
        //        .Should().Be( CKSetupRunResult.UpToDate );

        //    conf.ExternalSignature = "something";
        //    TestHelper.CKSetup.Run( conf, storePath, localRunner ? "none" : null, launchDebug: false, forceSetup: false )
        //        .Should().Be( CKSetupRunResult.Succeed );
        //}

    }
}
