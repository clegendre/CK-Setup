
using System;

[assembly: CK.Setup.IsModel()]
[assembly: CK.Setup.RequiredSetupDependency( "StupidEngine", null )]

namespace CK.Setup
{
    class IsModelAttribute : Attribute
    {
    }

    class RequiredSetupDependencyAttribute : Attribute
    {
        public RequiredSetupDependencyAttribute( string assemblyName, string minDependencyVersion )
        {
        }
    }


}
